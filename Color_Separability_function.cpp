
//delete 'return SUCCESSFUL_RETURN;' after //premature exit for testing!

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
//#include "Multifractal_SelectedFeas_2.h"
#include "Color_Separability.h"

using namespace imago;

FILE *fout_lr;
FILE *fout_PrintFeatures;

int
	nNumOfOneFea_Spectrum_AdjustedGlob = -1,
		nNumOfSpectrumFea_Glob = -1,

	nNumOfBinCur_Glob,
	iFea_Glob,

	nNumfPixelsWithIntensityLessThanZero_AfterConvolutionInOneDirection_Glob = 0,
	nNumfPixelsWithIntensityMoreThan255_AfterConvolutionInOneDirection_Glob = 0,

	iIntensity_Interval_1_Glob,
	iIntensity_Interval_2_Glob;

float
	fPercentageOfPixelsWithIntensityOffTheLimits_Glob;

//#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.cpp"
#include "Color_Separability_FunctOnly.cpp"

////////////////////////////////////////////////

int Reading_AColorImage(
	const Image& image_in,

	COLOR_IMAGE *sColor_Image)
{
	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

#ifdef USING_HISTOGRAM_EQUALIZATION
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);
#endif //#ifdef USING_HISTOGRAM_EQUALIZATION


	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nImageWidth,
		nImageHeight;
	////////////////////////////////////////////////////////////////////////

//initialization
	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Reading_AColorImage': nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n  'Reading_AColorImage': nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image width: nImageWidth = %d > nLenMax = %d  ...", nImageWidth, nLenMax);
		getchar(); exit(1);

		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_PrintFeatures, "\n\n An error in reading the image height:nImageHeight = %d > nWidMax = %d", nImageHeight, nWidMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image height: nImageHeight = %d > nWidMax = %d...", nImageHeight, nWidMax);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	//Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	//COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		//&sColor_Image); // COLOR_IMAGE *sColor_Imagef);
		sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Reading_AColorImage':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in 'Reading_AColorImage':");
		getchar(); exit(1);

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)
/////////////////////////////////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
} //int Reading_AColorImage(...
////////////////////////////////////////////////////////////////////

int NumOfPixelsIn_ColorBins(
	const int nImageWidthf,
	const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef, //[]

	int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
	int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
	int nNumOfPixelsIn_BlueBinsArrf[],
	
	int nNumOfPixelsIn_CombinedColorBinsArrf[]) //[nNumOfBins_OneImage_AllColors]
{
	int
		nIndexOfPixelCurf,

		nIndexOfBinCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,

		iFeaf,
		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedBinf,
		nGreenBinf,
		nBlueBinf,

		iWidf,
		iLenf;
////////////////////////////////
	for (iFeaf = 0; iFeaf < nNumOfBins_OneColor; iFeaf++)
	{
		nNumOfPixelsIn_RedBinsArrf[iFeaf] = 0;
		nNumOfPixelsIn_GreenBinsArrf[iFeaf] = 0;
		nNumOfPixelsIn_BlueBinsArrf[iFeaf] = 0;
	} //for (iFeaf = 0; iFeaf < nNumOfBins_OneColor; iFeaf++)
//////////////////////////

	for (iFeaf = 0; iFeaf < nNumOfBins_OneImage_AllColors; iFeaf++)
	{
		nNumOfPixelsIn_CombinedColorBinsArrf[iFeaf] = 0;
	} //for (iFeaf = 0; iFeaf < v; iFeaf++)
/////////////////////////////

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nRedBinf = nRedCurf / nBinSizeForColorSepar;
			nGreenBinf = nGreenCurf / nBinSizeForColorSepar;
			nBlueBinf = nBlueCurf / nBinSizeForColorSepar;

			if (nRedBinf < 0 || nRedBinf >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins': nRedBinf = %d >= nNumOfBins_OneColor = %d, iLenf = %d, iWidf = %d", 
					nRedBinf, nNumOfBins_OneColor, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nRedBinf < 0 || nRedBinf >= nNumOfBins_OneColor)

			if (nGreenBinf < 0 || nRedBinf >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins': nGreenBinf = %d >= nNumOfBins_OneColor = %d, iLenf = %d, iWidf = %d",
					nGreenBinf, nNumOfBins_OneColor, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nGreenBinf < 0 || nGreenBinf >= nNumOfBins_OneColor)

			if (nBlueBinf < 0 || nBlueBinf >= nNumOfBins_OneColor)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins': nBlueBinf = %d >= nNumOfBins_OneColor = %d, iLenf = %d, iWidf = %d",
					nBlueBinf, nNumOfBins_OneColor, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nBlueBinf < 0 || nBlueBinf >= nNumOfBins_OneColor)

			nNumOfPixelsIn_RedBinsArrf[nRedBinf] += 1;
			nNumOfPixelsIn_GreenBinsArrf[nGreenBinf] += 1;
			nNumOfPixelsIn_BlueBinsArrf[nBlueBinf] += 1;

			nIndexOfBinCurf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) +( nBlueBinf * (nNumOfBins_OneColor* nNumOfBins_OneColor));

			if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)
			{
				printf("\n\n An error in 'NumOfPixelsIn_ColorBins': nIndexOfBinCurf = %d >= nNumOfBins_OneImage_AllColors = %d, iLenf = %d, iWidf = %d",
					nIndexOfBinCurf, nNumOfBins_OneImage_AllColors, iLenf, iWidf);
				printf("\n\n Please press any key to exit");			getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfBinCurf < 0 || nIndexOfBinCurf >= nNumOfBins_OneImage_AllColors)

			nNumOfPixelsIn_CombinedColorBinsArrf[nIndexOfBinCurf] += 1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int NumOfPixelsIn_ColorBins(...

////////////////////////////////////
int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Green = nIntensityStatMax; // -1;
	sColor_Imagef->nIntensityOfBackground_Blue = nIntensityStatMax; // -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;

	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
		{
			return 0.0; //instead of 1.0
			//return SUCCESSFUL_RETURN; -- should return a float number
		} //
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = (float)(1.0) / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(
//////////////////////////////////////////////////////////

int Initializing_Grayscale_Image(
	//const int nImageWidthf,
	//const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef,
	GRAYSCALE_IMAGE *sGrayscale_Imagef)
{
	int
		nIndexOfPixelCurf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,
		iWidf,
		iLenf,

		nRedf,
		nGreenf,
		nBluef,

		nImageSizeCurf = nImageWidthf * nImageLengthf;

	sGrayscale_Imagef->nWidth = nImageWidthf;
	sGrayscale_Imagef->nLength = nImageLengthf;

	//printf("\n Initializing_Grayscale_Image: 1, please press any key"); getchar();

	////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n ///////////////////////////////////////////////////////////////////");
	printf("\n 'Initializing_Grayscale_Image': nImageWidthf = %d, nImageLengthf = %d\n", nImageWidthf, nImageLengthf);

	//fprintf(fout_lr, "\n\n ///////////////////////////////////////////////////////////////////");
	//fprintf(fout_lr, "\n 'Initializing_Grayscale_Image':\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	sGrayscale_Imagef->nPixelArr = new int[nImageSizeCurf];

	if (sGrayscale_Imagef->nPixelArr == nullptr)
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		//fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'Initializing_Grayscale_Image'");
		printf("\n\n Please press any key:"); getchar();
		//#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (sGrayscale_Imagef->nPixelArr == nullptr)

	//printf("\n Initializing_Grayscale_Image: 2, please press any key"); getchar();

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//printf("\n iWidf = %d, nImageWidthf = %d", iWidf, nImageWidthf); getchar();
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			//the average of colors
			sGrayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef) / 3;
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	//printf("\n Initializing_Grayscale_Image: the end, please press any key"); getchar();
	return SUCCESSFUL_RETURN;
}//int Initializing_Grayscale_Image(...
//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////

void Initializing_AFloatVec_To_Zero(
	const int nDimf,
	float fVecArrf[]) //[nDimf]
{
	int
		iFeaf;

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fVecArrf[iFeaf] = 0.0;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
}//void Initializing_AFloatVec_To_Zero(
////////////////////////////////////////////////////////////////////////

int	Converting_All_Feas_To_ARange_0_1(
	const int nDimf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	int &nNumOfFeasWithUnusualValuesf,
	int &nNumOfFeasWith_TheSameValuesf,

	int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
	float fFeas_InitMinArrf[], //[nDimf]
	float fFeas_InitMaxArrf[], //[nDimf]

	float fArr1stf[], //[nDimf*nNumVec1stf]
	float fArr2ndf[]) //[nDimf*nNumVec2ndf]
{

	int
		nNumOfFeasFilledf = 0,

		nIndexf,
		nTempf,
		iVecf,
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaCurf;

	nNumOfFeasWithUnusualValuesf = 0;
	nNumOfFeasWith_TheSameValuesf = 0;
	//////////////////////////////////////

	float *fFeaRangeArrf;
	fFeaRangeArrf = new float[nDimf];

	if (fFeaRangeArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeaRangeArrf' in 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeaRangeArrf == nullptr)
	
		fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': nDimf = %d, nNumVec1stf = %d, nNumVec2ndf = %d", nDimf, nNumVec1stf, nNumVec2ndf);
		fflush(fout_PrintFeatures);

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nFeaTheSameOrNotArrf[iFeaf] = 0; //not the same initially
		fFeaRangeArrf[iFeaf] = 0.0;

		fFeas_InitMinArrf[iFeaf] = fLarge;
		fFeas_InitMaxArrf[iFeaf] = -fLarge;
	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

//printing all fea vecs+

	/*
	fprintf(fout_PrintFeatures, "\n\n Normal:");

for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
{
	fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': 1 (init feas):  iVecf = %d, ", iVecf);
	nTempf = iVecf * nDimf;
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		nIndexf = iFeaf + nTempf;
		fFeaCurf = fArr1stf[nIndexf];

		fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

//fprintf(fout_PrintFeatures, "\n\n Malignant:");

for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
{
	fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': 2 (init feas):  iVecf = %d, ", iVecf);
	nTempf = iVecf * nDimf;
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{

		nIndexf = iFeaf + nTempf;
		fFeaCurf = fArr2ndf[nIndexf];

		fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);

	}//for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
*/

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr1stf[nIndexf];

			if (fFeaCurf < -fLarge || fFeaCurf > fLarge)
			{
				printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fFeaCurf = %E, fLarge = %E, iFeaf = %d, iVecf = %d", fFeaCurf, fLarge, iFeaf, iVecf);
				printf("\n\n Please press any key to exit"); getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (fFeaCurf < -fLarge || fFeaCurf > fLarge)

			if (iFeaf == nNumOfSelectedFea)
			{
				//fprintf(fout_PrintFeatures, "\n 1: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout_PrintFeatures, "\n\n 1: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//fprintf(fout_PrintFeatures, "\n\n 1: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])

		} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
//////////////////////////////////////////////////////////
		//fprintf(fout_PrintFeatures, "\n\n");
		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			nTempf = iVecf * nDimf;
			nIndexf = iFeaf + nTempf;
			fFeaCurf = fArr2ndf[nIndexf];

			if (fFeaCurf < -fLarge || fFeaCurf > fLarge)
			{
				printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fFeaCurf = %E, fLarge = %E, iFeaf = %d, iVecf = %d", fFeaCurf, fLarge, iFeaf, iVecf);
				printf("\n\n Please press any key to exit"); getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			} //if (fFeaCurf < -fLarge || fFeaCurf > fLarge)

			if (iFeaf == nNumOfSelectedFea)
			{
				//	fprintf(fout_PrintFeatures, "\n 2: iFeaf = %d, iVecf = %d, fFeaCurf = %E", iFeaf, iVecf, fFeaCurf);
			} //if (iFeaf == nNumOfSelectedFea)

			if (fFeaCurf < fFeas_InitMinArrf[iFeaf])
			{
				fFeas_InitMinArrf[iFeaf] = fFeaCurf;
				if (iFeaf == nNumOfSelectedFea)
				{
					//	fprintf(fout_PrintFeatures, "\n\n 2: fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)

			} //if (fFeaCurf < fFeas_InitMinArrf[iFeaf])

			if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
			{
				fFeas_InitMaxArrf[iFeaf] = fFeaCurf;

				if (iFeaf == nNumOfSelectedFea)
				{
					//		fprintf(fout_PrintFeatures, "\n\n 2: fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				} //if (iFeaf == nNumOfSelectedFea)
			} //if (fFeaCurf > fFeas_InitMaxArrf[iFeaf])
		} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)
		{
			nNumOfFeasWithUnusualValuesf += 1;
			//printf("\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);
			//fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %E", iFeaf, fFeas_InitMinArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 1: an unusual fea, fFeas_InitMinArrf[%d] = %d", iFeaf, fFeas_InitMinArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (fFeas_InitMinArrf[iFeaf] < -fFeaValueThreshold_ForWarning)

		if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
		{
			if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)
			{
				nNumOfFeasWithUnusualValuesf += 1;
				//printf("\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);
				//fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %E", iFeaf, fFeas_InitMaxArrf[iFeaf]);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Converting_All_Feas_To_ARange_0_1' 2: an unusual fea, fFeas_InitMaxArrf[%d] = %d", iFeaf, fFeas_InitMaxArrf[iFeaf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (fFeas_InitMinArrf[iFeaf] >= -fFeaValueThreshold_ForWarning)

		}//if (fFeas_InitMaxArrf[iFeaf] > fFeaValueThreshold_ForWarning)
	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
////////////////////////////////////////////////////////////

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': the total # of unusual feas, nNumOfFeasWithUnusualValuesf = %d", nNumOfFeasWithUnusualValuesf);
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		fRangeCurf = fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf];

		if (iFeaf == nNumOfSelectedFea)
		{
			//		printf( "\n\n fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
				//		fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout_PrintFeatures, "\n 'Converting_All_Feas_To_ARange_0_1': fRangeCurf = %E, fFeas_InitMaxArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//fRangeCurf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

		} //if (iFeaf == nNumOfSelectedFea)

		if (fRangeCurf < feps)
		{
			nFeaTheSameOrNotArrf[iFeaf] = 1; //the same 
			nNumOfFeasWith_TheSameValuesf += 1;

			fprintf(fout_PrintFeatures, "\n The next nNumOfFeasWith_TheSameValuesf = %d,  (fFeas_InitMaxArrf[%d] = %E - fFeas_InitMinArrf[%d]) = %E = %E",
				nNumOfFeasWith_TheSameValuesf, iFeaf, fFeas_InitMaxArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf], fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf]);

		} //if (fFeas_InitMaxArrf[iFeaf] - fFeas_InitMinArrf[iFeaf] < feps)
		else
		{
			fFeaRangeArrf[iFeaf] = fRangeCurf;
		} //else

	} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	printf("\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);
	fprintf(fout_PrintFeatures, "\n\n 'Converting_All_Feas_To_ARange_0_1': the total nNumOfFeasWith_TheSameValuesf = %d, nDimf = %d", nNumOfFeasWith_TheSameValuesf, nDimf);

	fflush(fout_PrintFeatures);
	/////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
	{
		if (nFeaTheSameOrNotArrf[iFeaf] == 0)  //the feas are different 
		{
			//	fprintf(fout_PrintFeatures, "\n");
			for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr1stf[nIndexf] = (fArr1stf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

							//fprintf(fout_PrintFeatures, "\n  'Converting_All_Feas_To_ARange_0_1' 1: iFeaf = %d, iVecf = %d, fArr1stf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
								//iFeaf, iVecf,nIndexf,fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)

				if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 1: fArr1stf[%d] = %E is out of the range", nIndexf, fArr1stf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					fflush(fout_PrintFeatures);

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] fFeaRangeArrf;
					return UNSUCCESSFUL_RETURN;
				}//if (fArr1stf[nIndexf] < -feps || fArr1stf[nIndexf] > fNormalizedRangeMaxf)

			} // for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

			//fprintf(fout_PrintFeatures, "\n");
			for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
			{
				nTempf = iVecf * nDimf;
				nIndexf = iFeaf + nTempf;

				fArr2ndf[nIndexf] = (fArr2ndf[nIndexf] - fFeas_InitMinArrf[iFeaf]) / fFeaRangeArrf[iFeaf];

				if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)
				{
					printf("\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					printf("\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Converting_All_Feas_To_ARange_0_1' 2: fArr2ndf[%d] = %E is out of the range", nIndexf, fArr2ndf[nIndexf]);
					fprintf(fout_lr, "\n iFeaf = %d, iVecf = %d", iFeaf, iVecf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit:");	getchar(); exit(1);
					delete[] fFeaRangeArrf;

					return UNSUCCESSFUL_RETURN;
				}//if (fArr2ndf[nIndexf] < -feps || fArr2ndf[nIndexf] > fNormalizedRangeMaxf)

				if (iFeaf == nNumOfSelectedFea)
				{
					//		printf( "\n\n fArr1stf[nIndexf] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//		fArr1stf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

					//fprintf(fout_PrintFeatures, "\n  'Converting_All_Feas_To_ARange_0_1' 2: iFeaf = %d, iVecf = %d, fArr2ndf[%d] = %E, fFeaRangeArrf[%d] = %E,  fFeas_InitMinArrf[%d] = %E ",
						//iFeaf, iVecf, nIndexf, fArr2ndf[nIndexf], iFeaf, fFeaRangeArrf[iFeaf], iFeaf, fFeas_InitMinArrf[iFeaf]);

				} //if (iFeaf == nNumOfSelectedFea)
			} // for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

			fflush(fout_PrintFeatures);
		} //if (nFeaTheSameOrNotArrf[iFeaf] == 0)  //the feas are different 

	} // for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

	delete[] fFeaRangeArrf;

	return SUCCESSFUL_RETURN;
} // int Converting_All_Feas_To_ARange_0_1(...
////////////////////////////////////////////////////////////////////////////////////////////

int ConvertingVecs_ToBest_SeparableFeas(
	const float fLargef,
	const  float fepsf,

	const int nDimf,
	const int nDimSelecMaxf,

	const int nNumVec1stf,
	const int nNumVec2ndf,

	const float fPrecisionOf_G_Const_Searchf,

	const  float fBorderBelf,
	const  float fBorderAbof,

	const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
	const float fEfficBestSeparOfOneFeaAcceptMaxf,

	float fArr1stf[], //[nDimf*nNumVec1stf] // will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
	float fArr2ndf[], //[nDimf*nNumVec2ndf] //will be converted to (0,1) by 'Converting_All_Feas_To_ARange_0_1' --> 0 -- 1 
///////////////////////////////////

	int &nDimSelecf,

	float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf*nNumVec1stf]
	float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf*nNumVec2ndf]

	float &fRatioBestMinf,

	int &nPosOneFeaBestMaxf,

	int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

	float &fNumArr1stSeparBestMaxf,
	float &fNumArr2ndSeparBestMaxf,

	float fRatioBestArrf[], //nDimSelecMaxf

	int nPosFeaSeparBestArrf[])	//nDimSelecMaxf
{
	int	Converting_All_Feas_To_ARange_0_1(
		const int nDimf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		int &nNumOfFeasWithUnusualValuesf,
		int &nNumOfFeasWith_TheSameValuesf,

		int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		float fFeas_InitMinArrf[], //[nDimf]
		float fFeas_InitMaxArrf[], //[nDimf]

		float fArr1stf[], //[nDimf*nNumVec1stf]
		float fArr2ndf[]); //[nDimf*nNumVec2ndf]

	int SelectingBestFeas(
		const float fLargef,
		const  float fepsf,

		const int nDimf,
		const int nDimSelecMaxf,

		const int nNumVec1stf,
		const int nNumVec2ndf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		const float fEfficBestSeparOfOneFeaAcceptMaxf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1
///////////////////////////////////

int &nDimSelecf,
//float fSelecArr1stf[], //0 -- 1 //[nDimSelecMaxf]
//float fSelecArr2ndf[], //0 -- 1 //[nDimSelecMaxf]

		float &fRatioBestMinf,

		int &nPosOneFeaBestMaxf,

		int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestMaxf,
		float &fNumArr2ndSeparBestMaxf,

		float fRatioBestArrf[], //nDimSelecMaxf

		int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

//////////////////////////////////////////
	int
		nResf,
		iFeaf,
		iVecf,
		nIndexf,
		nTempf,
		nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf;

	//nFeaTheSameOrNotArrf[nDim]; //[nDimf] // 1-- the same, 0-- not

	float
		fFeaCurf;
	//fFeas_InitMinArrf[nDim],
	//fFeas_InitMaxArrf[nDim];

	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': begin");
	fflush(fout_PrintFeatures);

////////////////////////////////////////////
	int *nFeaTheSameOrNotArrf = new int[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (nFeaTheSameOrNotArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'nFeaTheSameOrNotArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (nFeaTheSameOrNotArrf == nullptr)

	float *fFeas_InitMinArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (fFeas_InitMinArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMinArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMinArrf == nullptr)

	float *fFeas_InitMaxArrf = new float[nDimf]; //0 -- 1 //[nDimSelecMaxf]

	if (fFeas_InitMaxArrf == nullptr)
	{
		printf("\n\n An error in dynamic memory allocation for 'fFeas_InitMaxArrf'");
		printf("\n Please press any key to exit"); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fFeas_InitMaxArrf == nullptr)


/////////////////////////////////////////
	printf("\n\n ConvertingVecs_ToBest_SeparableFeas': nDimf = %d, nDimSelecMaxf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nDimSelecMaxf, nNumVec1stf, nNumVec2ndf);

	fprintf(fout_PrintFeatures, "\n\n 'ConvertingVecs_ToBest_SeparableFeas': nDimf = %d, nDimSelecMaxf = %d, nNumVec1stf = %d, nNumVec2ndf = %d",
		nDimf, nDimSelecMaxf, nNumVec1stf, nNumVec2ndf);
	fflush(fout_PrintFeatures);

	/*
		fprintf(fout_PrintFeatures, "\n\n 'ConvertingVecs_ToBest_SeparableFeas': entry data");
		for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)
		{
			fprintf(fout_PrintFeatures, "\n1, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr1stf[nIndexf];

				fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec1stf; iVecf++)

		for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)
		{
			fprintf(fout_PrintFeatures, "\n0, ");
			nTempf = iVecf * nDimf;
			for (iFeaf = 0; iFeaf < nDimf; iFeaf++)
			{
				nIndexf = iFeaf + nTempf;
				fFeaCurf = fArr2ndf[nIndexf];

				fprintf(fout_PrintFeatures, "%d:%E ", iFeaf, fFeaCurf);
			} //for (iFeaf = 0; iFeaf < nDimf; iFeaf++)

		} //for (iVecf = 0; iVecf < nNumVec2ndf; iVecf++)

		fflush(fout_PrintFeatures);
	*/
	//printf("\n\n Please press any key:"); getchar();

	nResf = Converting_All_Feas_To_ARange_0_1(
		nDimf, //const int nDimf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		nNumOfFeasWithUnusualValuesf, //int &nNumOfFeasWithUnusualValuesf,
		nNumOfFeasWith_TheSameValuesf, //int &nNumOfFeasWith_TheSameValuesf,

		nFeaTheSameOrNotArrf, //int nFeaTheSameOrNotArrf[], //[nDimf] // 1-- the same, 0-- not
		fFeas_InitMinArrf, //float fFeas_InitMinArrf[], //[nDimf]
		fFeas_InitMaxArrf, //float fFeas_InitMaxArrf[], //[nDimf]

		fArr1stf, //float fArr1stf[], //[nDimf*nNumVec1stf]  //0 -- 1 now
		fArr2ndf); // float fArr2ndf[]); //[nDimf*nNumVec2ndf]  //0 -- 1 now

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_All_Feas_To_ARange_0_1'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	fprintf(fout_PrintFeatures,"\n\n ConvertingVecs_ToBest_SeparableFeas': after 'Converting_All_Feas_To_ARange_0_1'");
	fflush(fout_PrintFeatures);

	//printf("\n\n Please press any key:"); getchar();

	nResf = SelectingBestFeas(
		fLargef, //const float fLargef,
		fepsf, //const  float fepsf,

		nDimf, //const int nDimf,
		nDimSelecMaxf, //const int nDimSelecMaxf,

		nNumVec1stf, //const int nNumVec1stf,
		nNumVec2ndf, //const int nNumVec2ndf,

		fPrecisionOf_G_Const_Searchf, //const float fPrecisionOf_G_Const_Searchf,

		0.0, //const  float fBorderBelf,
		1.0, //const  float fBorderAbof,

		nNumIterMaxForFindingBestSeparByRatioOfOneFeaf, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,
		fEfficBestSeparOfOneFeaAcceptMaxf, //const float fEfficBestSeparOfOneFeaAcceptMaxf,

		fArr1stf, //const float fArr1stf[], //0 -- 1
		fArr2ndf, //const  float fArr2ndf[], //0 -- 1
		///////////////////////////////////

		nDimSelecf, //int &nDimSelecf,

		fRatioBestMinf, //float &fRatioBestMinf,

		nPosOneFeaBestMaxf, //int &nPosOneFeaBestMaxf,

		nSeparDirectionBestMaxf, //int &nSeparDirectionBestMaxf, //1 -- Abo, -1 -- Bel

		fNumArr1stSeparBestMaxf, //float &fNumArr1stSeparBestMaxf,
		fNumArr2ndSeparBestMaxf, //float &fNumArr2ndSeparBestMaxf,

		fRatioBestArrf, //float fRatioBestArrf[], //nDimSelecMaxf

		nPosFeaSeparBestArrf); // int nPosFeaSeparBestArrf[]); //nDimSelecMaxf

	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': after 'SelectingBestFeas, nResf = %d'", nResf);
	fflush(fout_PrintFeatures);


	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas': there are no features satisfying fEfficBestSeparOfOneFeaAcceptMax = %E",
			fEfficBestSeparOfOneFeaAcceptMax);
		fprintf(fout_lr, "\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax'; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar(); // exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	else if (nResf == SUCCESSFUL_RETURN)
	{
		printf("\n\n 'ConvertingVecs_ToBest_SeparableFeas' after 'SelectingBestFeas': the number of selected features  nDimSelecf = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Please increase 'fEfficBestSeparOfOneFeaAcceptMax' results to get a larger number of the selected features; the range is (-2.0, -1.0) ");
		printf("\n\n 'UNSUCCESSFUL_RETURN': please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':  the number of selected features  nDimSelec = %d is less or equal to the specified number nDimSelecMax = %d",
			nDimSelec, nDimSelecMax);
		fprintf(fout_lr, "\n\n  Increasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a larger number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	}//else if (nResf == SUCCESSFUL_RETURN)
	else if (nResf == 2)
	{
		printf("\n\n After 'SelectingBestFeas': the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);
		printf("\n\n Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n After 'SelectingBestFeas':the number of selected features nDimSelecf = %d is greater than the specified number nDimSelecMax = %d that is Ok",
			nDimSelecf, nDimSelecMax);

		fprintf(fout_lr, "\n\n  Decreasing 'fEfficBestSeparOfOneFeaAcceptMax' results in a smaller number of the selected features; the range is (-2.0, -1.0) ");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	}//else if (nResf == 2)

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	printf("\n\n After 'SelectingBestFeas': fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, initial nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
	//printf("\n\n Please press any key:"); getchar();

#ifndef COMMENT_OUT_ALL_PRINTS

	fprintf(fout_lr, "\n\n After 'SelectingBestFeas':fRatioBestMinf = %E, nPosOneFeaBestMaxf = %d, iniatal nDimSelecf = %d, nDimSelecMax = %d",
		fRatioBestMinf, nPosOneFeaBestMaxf, nDimSelecf, nDimSelecMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/*
	if (nDimSelecf > nDimSelecMax)
	{
		printf("\n\n  Changing nDimSelecf = %d to nDimSelecMax = %d", nDimSelecf, nDimSelecMax);

	#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n  Changing nDimSelec = %d to nDimSelecMax = %d", nDimSelec, nDimSelecMax);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nDimSelecf = nDimSelecMax;
	}//if (nDimSelec > nDimSelecMax)
	*/

	fprintf(fout_PrintFeatures, "\n\n //////////////////////////////////////////////////////////////////////");

	//for (int iFeaf = 0; iFeaf < nDimSelecf; iFeaf++)
	for (iFeaf = 0; iFeaf < nDimSelecMax; iFeaf++)
	{
		printf("\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
		//fprintf(fout_lr, "\n\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			//iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);

		fprintf(fout_PrintFeatures, "\n After 'SelectingBestFeas': fRatioBestArrf[%d] = %E, nPosFeaSeparBestArrf[%d] = %d",
			iFeaf, fRatioBestArrf[iFeaf], iFeaf, nPosFeaSeparBestArrf[iFeaf]);
	} //for (iFea = 0; iFea < nDimSelecMax; iFea++)


	//printf("\n\n Before 'Converting_Arr_To_Selec': please press any key"); fflush(fout_PrintFeatures); getchar();
	//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': before 'Converting_Arr_To_Selec' (normal)");
	fflush(fout_PrintFeatures);

	//float *fVecTrainSelec_1st = new float[nNumVecTrain_1st*nDimSelecf]; //0 -- 1 //[nDimSelecMaxf]
	nResf = Converting_Arr_To_Selec(
		nDimf, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Normal, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr1stf, //const float fVecArr[],[nProdTrain_1st]
		fSelecArr1stf); //float fVecSelecArr[]) //[nNumVecTrain_1st*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 1:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);

		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)
/////////////////////////////
	fprintf(fout_PrintFeatures, "\n\n ConvertingVecs_ToBest_SeparableFeas': before 'Converting_Arr_To_Selec' (malignant)");
	fflush(fout_PrintFeatures);

	nResf = Converting_Arr_To_Selec(
		nDimf, //const int nDimf,
		nDimSelecMax, //const int nDimSelecf,

		nNumOfVecs_Malignant, //const int nNumVecf,

		nPosFeaSeparBestArrf, //const int nPosFeaSeparBestArr[], //[nDimSelecf]

		fArr2ndf, //const float fVecArr[],[nProdTrain_2nd]
		fSelecArr2ndf); //float fVecSelecArr[]) //[nNumVecTrain_2nd*nDimSelec]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		printf("\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
		printf("\n\n Please press any key to exit");

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'ConvertingVecs_ToBest_SeparableFeas' by 'Converting_Arr_To_Selec' 2:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		getchar(); exit(1);
		delete[] nFeaTheSameOrNotArrf;
		delete[] fFeas_InitMinArrf;
		delete[] fFeas_InitMaxArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

	//printf("\n\n The end in 'ConvertingVecs_ToBest_SeparableFeas'");
	//printf("\n\n Please press any key"); getchar();

	delete[] fFeas_InitMinArrf;
	delete[] fFeas_InitMaxArrf;

	delete[] nFeaTheSameOrNotArrf;

	return SUCCESSFUL_RETURN;
} //int ConvertingVecs_ToBest_SeparableFeas(...
////////////////////////////////////////////////////////////////////////////////////////////

int	Converting_One_Fea_To_ARange_0_1(
	const int nDim1f,
	const int nDim2f,

	int &nFeaTheSameOrNotf,
	float &fFeaMinf,
	float &fFeaMaxf,

	float fOneDimArr1stf[], //[nDim1f]
	float fOneDimArr2ndf[]) //[nDim2f]
{

	int
		iFeaf;

	float
		fRangeCurf,
		fNormalizedRangeMaxf = 1.0 + feps,
		fFeaCurf;

	//////////////////////////////////////

		//fprintf(fout_PrintFeatures, "\n\n 'Converting_One_Fea_To_ARange_0_1': nDim1f = %d, nDim2f = %d", nDim1f, nDim2f);
	nFeaTheSameOrNotf = 0; //not the same initially

	fFeaMinf = fLarge;
	fFeaMaxf = -fLarge;

	for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
	{
		fFeaCurf = fOneDimArr1stf[iFeaf];

		if (fFeaCurf < fFeaMinf)
		{
			fFeaMinf = fFeaCurf;
		} //if (fFeaCurf < fFeaMinf)

		if (fFeaCurf > fFeaMaxf)
		{
			fFeaMaxf = fFeaCurf;
		} //if (fFeaCurf > fFeaMinf)

	} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)

//////////////////////////////////////////////////////////
	for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
	{
		fFeaCurf = fOneDimArr2ndf[iFeaf];

		if (fFeaCurf < fFeaMinf)
		{
			fFeaMinf = fFeaCurf;
		} //if (fFeaCurf < fFeaMinf)

		if (fFeaCurf > fFeaMaxf)
		{
			fFeaMaxf = fFeaCurf;
		} //if (fFeaCurf > fFeaMinf)

	} // for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
////////////////////////////////////////////////////////////

	fRangeCurf = fFeaMaxf - fFeaMinf;

	if (fRangeCurf < feps)
	{
		nFeaTheSameOrNotf = 1;

		return (-2);
	} //if (fRangeCurf < feps)

/////////////////////////////////////
	if (nFeaTheSameOrNotf == 0)  //the feas are different 
	{
		//	fprintf(fout_PrintFeatures, "\n");
		for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
		{
			fOneDimArr1stf[iFeaf] = (fOneDimArr1stf[iFeaf] - fFeaMinf) / fRangeCurf;

			if (fOneDimArr1stf[iFeaf] < -feps || fOneDimArr1stf[iFeaf] > fNormalizedRangeMaxf)
			{
				printf("\n\n An error in 'Converting_One_Fea_To_ARange_0_1' 1: fOneDimArr1stf[%d] = %E is out of the range", iFeaf, fOneDimArr1stf[iFeaf]);

				printf("\n\n Please press any key to exit:");	fflush(fout_PrintFeatures);  getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (fOneDimArr1stf[nIndexf] < -feps || fOneDimArr1stf[nIndexf] > fNormalizedRangeMaxf)

		} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)
////////////////////////////////////////////
		for (iFeaf = 0; iFeaf < nDim2f; iFeaf++)
		{
			fOneDimArr2ndf[iFeaf] = (fOneDimArr2ndf[iFeaf] - fFeaMinf) / fRangeCurf;


			if (fOneDimArr2ndf[iFeaf] < -feps || fOneDimArr2ndf[iFeaf] > fNormalizedRangeMaxf)
			{
				printf("\n\n An error in 'Converting_One_Fea_To_ARange_0_1' 1: fOneDimArr2ndf[%d] = %E is out of the range", iFeaf, fOneDimArr2ndf[iFeaf]);

				printf("\n\n Please press any key to exit:");	fflush(fout_PrintFeatures);  getchar(); exit(1);
				return UNSUCCESSFUL_RETURN;
			}//if (fOneDimArr2ndf[nIndexf] < -feps || fOneDimArr2ndf[nIndexf] > fNormalizedRangeMaxf)

		} // for (iFeaf = 0; iFeaf < nDim1f; iFeaf++)

		fflush(fout_PrintFeatures);
	} //if (nFeaTheSameOrNotf == 0)  //the feas are different 

	return SUCCESSFUL_RETURN;
} // int Converting_One_Fea_To_ARange_0_1(...
//////////////////////////////////////////////////////////////

/*
nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot * nNumOfBins_OneImage_AllColors);

nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBinsArr[iBin];

//[nNumOfBins_AllColorsForAll_NorImages = nNumOfBins_OneImage_AllColors * nNumOfVecs_Normal
fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBinsArr[nIndexOfBin]) / (float)(nImageAreaCur);

*/
int Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
	const int nDimf, // == nNumOfBins_OneImage_AllColors

	const int nNumOfVecsTotf,
	const int nNumOfOneSelecFeaf,

	const float fFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

	float fFeas_OneVec_Arrf[]) //[nNumOfVecsTotf]
{
	int
		nIndexf,
		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,

		iVecf;
////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nNumOfOneSelecFeaf = %d", 
		nDimf, nNumOfVecsTotf, nNumOfOneSelecFeaf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
	{
		nIndexf = nNumOfOneSelecFeaf + (iVecf*nDimf);
		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		//if (nVecf < 4)
		{
			//	fprintf(fout, "\n 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nNumOfOneSelecFeaf = %d, iVecf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nNumOfOneSelecFeaf, iVecf,nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		fFeas_OneVec_Arrf[iVecf] = fFeas_All_Arrf[nIndexf];
	} // for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

	return SUCCESSFUL_RETURN;
}// int Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs{...
///////////////////////////////////////////////////////

int Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
	const int nDimf,
	const int nNumOfVecsTotf,
	const int nNumOfOneSelecFeaf,

	const int nFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

	int nFeas_OneVec_Arrf[]) //[nNumOfVecsTotf]
{
	int
		nIndexf,
		nIndexMaxf = (nDimf* nNumOfVecsTotf) - 1,

		iVecf;
	////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nVecf = %d", nDimf, nNumOfVecsTotf, nVecf);
	fprintf(fout, "\n\n  'Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nDimf = %d, nNumOfVecsTotf = %d, nNumOfOneSelecFeaf = %d",
		nDimf, nNumOfVecsTotf, nNumOfOneSelecFeaf);
	fprintf(fout, "\n\n iVec_Train_Glob = %d, nY_Train_Actual_Glob = %d", iVec_Train_Glob, nY_Train_Actual_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)
	{
		nIndexf = nNumOfOneSelecFeaf + (iVecf*nDimf);
		if (nIndexf > nIndexMaxf)
		{
			printf("\n\n An error in 'Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout, "\n\n  An error in 'Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nIndexf = %d > nIndexMaxf = %d", nIndexf, nIndexMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			getchar();	exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (nIndexf > nIndexMaxf)

#ifndef COMMENT_OUT_ALL_PRINTS
		//if (nVecf < 4)
		{
			//	fprintf(fout, "\n 'Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs': nNumOfOneSelecFeaf = %d, iVecf = %d, nIndexf = %d, fFeas_All_Arrf[nIndexf] = %E", nNumOfOneSelecFeaf, iVecf,nIndexf, fFeas_All_Arrf[nIndexf]);
		}//if (nVecf < 4)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nFeas_OneVec_Arrf[iVecf] = nFeas_All_Arrf[nIndexf];
	} // for (iVecf = 0; iVecf < nNumOfVecsTotf; iVecf++)

	return SUCCESSFUL_RETURN;
}// int Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs{...
//////////////////////////////////////////////////////////////////

int SeparabilityOf_ColorBins(

	const int nNumOfBins_OneImage_AllColorsf,
///////////////////////////////////////////////////////
	const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,
	
	const int nNumOf_NonZeroMalImagesMinf,

	const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

	const float fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf,

///////////////////////////////////////////////////
	const int nNumOfVecs_Norf,
	const int nNumOfVecs_Malf,

	const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

	const float fAllBinsAll_ColorsAver_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const float fAllBinsAll_ColorsAver_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
/////////////////////////////////////////////
	float &fBestBinSeparabilityf,

	int &nNumOfBins_InNonZeroNeg_Imagesf,

	int nPositOfBins_InNonZeroNeg_Imagesf[], //[nNumOfBins_InNonZeroMal_ImagesMax] 

	float &fLargestRatioOfBinsAboveTheMinRatioForSeparf)
{

/*
for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
{
	nAllBinsAll_Colors_NorImagesArr[iFea] = 0;

	fAllBinsAll_ColorsAver_NorImagesArr[iFea] = 0.0;
} // for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
//////////////////////////////

	for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
	{
		nIndexOfBin = iBin + nNumOfProcessedFiles_NormalTot * nNumOfBins_OneImage_AllColors;

		nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBinsArr[iBin];

		fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBinsArr[nIndexOfBin]) / (float)(nImageAreaCur);
	} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
*/

	int Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
		const int nDimf, // == nNumOfBins_OneImage_AllColors

		const int nNumOfVecsTotf,
		const int nNumOfOneSelecFeaf,

		const float fFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

		float fFeas_OneVec_Arrf[]); //[nNumOfVecsTotf]

	int Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
		const int nDimf,
		const int nNumOfVecsTotf,
		const int nNumOfOneSelecFeaf,

		const int nFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

		int nFeas_OneVec_Arrf[]); //[nNumOfVecsTotf]

	int FindingBestSeparByRatioOfOneFea(
		const float fLargef,
		const int nDim1stf,
		const int nDim2ndf,

		const  float fepsf,

		const float fPrecisionOf_G_Const_Searchf,

		const  float fBorderBelf,
		const  float fBorderAbof,

		const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,

		const float fArr1stf[], //0 -- 1
		const  float fArr2ndf[], //0 -- 1

		float &fEfficBestSeparOfOneFeaf,

		float &fPosSeparOfOneFeaBestf,

		int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		float &fNumArr1stSeparBestf,
		float &fNumArr2ndSeparBestf,

		float &fDiffSeparOfArr1stAndArr2ndBestf,
		float &fRatioOfSeparOfArr1stAndArr2ndBestf);

	int Converting_nBinOfAllColorsf_To_SeparateColors(
		const int nBinOfAllColorsf,
		int &nRedBinf,
		int &nGreenBinf,
		int &nBlueBinf);

//////////////////////////////
	int
		nResf,

		nFeaByRatioTheSameOrNotf, //int &nFeaTheSameOrNotf,
		nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
		nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

		nRedBinf,
		nGreenBinf,
		nBlueBinf,

		iVecf,

		nNumOfNonZeroBinsIn_NorImagesf,
		nNumOfNonZeroBinsIn_MalImagesf,

		nSumOfPixelsIn_NonZeroNorImagesf,
		nSumOfPixelsIn_NonZeroMalImagesf,

		nBinsNor_OneDimf[nNumOfVecs_Normal],
		nBinsMal_OneDimf[nNumOfVecs_Malignant],

		iBinOfAllColorsf;

	float
		fFeaByRatioMinf, //float &fFeaMinf,
		fFeaByRatioMaxf, //float &fFeaMaxf,

		fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaf,

		fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestf,


		fNumArr1stSeparBestf, //float &fNumArr1stSeparBestf,
		fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestf,

		fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestf,
		fRatioOfSeparOfArr1stAndArr2ndBestf, // float &fRat

		fRatioBest_Minf = fLarge,

		fRatioOfSumsOfPixelsOfNonZero_MalToNorf,

		fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf,
		fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf = -fLarge,

		fBinsNor_OneDimf[nNumOfVecs_Normal],
		fBinsMal_OneDimf[nNumOfVecs_Malignant];
//////////////////////////
//not calculated yet
	nNumOfBins_InNonZeroNeg_Imagesf = 0;

	fLargestRatioOfBinsAboveTheMinRatioForSeparf = -fLarge;

/////////////////////////////////////
	for (iBinOfAllColorsf = 0; iBinOfAllColorsf < nNumOfBins_InNonZeroMal_ImagesMax; iBinOfAllColorsf++)
	{
		nPositOfBins_InNonZeroNeg_Imagesf[iBinOfAllColorsf] = -1; //no suitable color bin yet
	} //for (iBinOfAllColorsf = 0; iBinOfAllColorsf < nNumOfBins_InNonZeroMal_ImagesMax; iBinOfAllColorsf++)

//////////////////////////////////
	for (iBinOfAllColorsf = 0; iBinOfAllColorsf < nNumOfBins_OneImage_AllColorsf; iBinOfAllColorsf++)
	{
		nNumOfBinCur_Glob = iBinOfAllColorsf;
///////////////////////////////////////
//float
		nResf = Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
			nNumOfBins_OneImage_AllColors, //const int nDimf,

			nNumOfVecs_Normal, //onst int nNumOfVecsTotf,
			
			iBinOfAllColorsf, //const int nNumOfOneVecf,

			fAllBinsAll_ColorsAver_NorImagesArrf, //const float fFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

			fBinsNor_OneDimf); // float fFeas_OneVec_Arrf[]); //[nNumOfVecsTotf]

		nResf = Extracting_A_FloatVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
			nNumOfBins_OneImage_AllColors, //const int nDimf,

			nNumOfVecs_Malignant, //onst int nNumOfVecsTotf,

			iBinOfAllColorsf, //const int nNumOfOneVecf,

			fAllBinsAll_ColorsAver_MalImagesArrf, //const float fFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

			fBinsMal_OneDimf); // float fFeas_OneVec_Arrf[]); //[nNumOfVecsTotf]
//////////////////////////////////////
//int
/*
	const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
*/
		nResf = Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
			nNumOfBins_OneImage_AllColors, //const int nDimf,
			nNumOfVecs_Normal, //const int nNumOfVecsTotf,
			
			iBinOfAllColorsf, //const int nNumOfOneSelecFeaf,

			nAllBinsAll_Colors_NorImagesArrf, //const int nFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

			nBinsNor_OneDimf); // int nFeas_OneVec_Arrf[]); //[nNumOfVecsTotf]

		nResf = Extracting_An_IntVecCorrespondingToOneFea_From_2DimArrOf_AllVecs(
			nNumOfBins_OneImage_AllColors, //const int nDimf,
			
			nNumOfVecs_Malignant, //const int nNumOfVecsTotf,

			iBinOfAllColorsf, //const int nNumOfOneSelecFeaf,

			nAllBinsAll_Colors_MalImagesArrf, //const int nFeas_All_Arrf[], //[nDimf*nNumOfVecsTotf]

			nBinsMal_OneDimf); // int nFeas_OneVec_Arrf[]); //[nNumOfVecsTotf]
///////////////////////////////

		nNumOfNonZeroBinsIn_NorImagesf = 0; 
		nSumOfPixelsIn_NonZeroNorImagesf = 0;
		for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)
		{
			//fprintf(fout_PrintFeatures, "\n Before 1, iBinOfAllColorsf = %d: fBinsNor_OneDimf[%d] = %E", iBinOfAllColorsf, iVecf, fBinsNor_OneDimf[iVecf]);
			if (nBinsNor_OneDimf[iVecf] > nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf)
			{
				nNumOfNonZeroBinsIn_NorImagesf += 1;
				nSumOfPixelsIn_NonZeroNorImagesf += nBinsNor_OneDimf[iVecf];
			} //if (nBinsNor_OneDimf[iVecf] > nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf)

		} //for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)
/////////////////////////////////////

		nNumOfNonZeroBinsIn_MalImagesf = 0;
		nSumOfPixelsIn_NonZeroMalImagesf = 0;
		for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
		{
			//fprintf(fout_PrintFeatures, "\n Before 2, iBinOfAllColorsf = %d: fBinsMal_OneDimf[%d] = %E", iBinOfAllColorsf, iVecf, fBinsMal_OneDimf[iVecf]);
			if (nBinsMal_OneDimf[iVecf] > nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf)
			{
				nNumOfNonZeroBinsIn_MalImagesf += 1;
				nSumOfPixelsIn_NonZeroMalImagesf += nBinsMal_OneDimf[iVecf];
			} //if (nBinsMal_OneDimf[iVecf] > nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf)

		} //for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)

		fprintf(fout_PrintFeatures, "\n\n nNumOfNonZeroBinsIn_NorImagesf = %d, nNumOfNonZeroBinsIn_MalImagesf = %d, iBinOfAllColorsf = %d", 
			nNumOfNonZeroBinsIn_NorImagesf, nNumOfNonZeroBinsIn_MalImagesf,iBinOfAllColorsf);

		if (nNumOfNonZeroBinsIn_MalImagesf > 0)
		{
			if (nNumOfNonZeroBinsIn_NorImagesf > 0) //always works if nSumOfPixelsIn_NonZeroNorImagesf > 0
			{
				fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf = (float)(nNumOfNonZeroBinsIn_MalImagesf) / (float)(nNumOfNonZeroBinsIn_NorImagesf);
			} //
			else
			{
				fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf = (float)(nNumOfVecs_Malignant);
			}//else
////////////////////////////////////////////////////////////////

			if (nSumOfPixelsIn_NonZeroMalImagesf > 0)
			{
				if (nSumOfPixelsIn_NonZeroNorImagesf > 0)
				{
					fRatioOfSumsOfPixelsOfNonZero_MalToNorf = (float)(nSumOfPixelsIn_NonZeroMalImagesf) / (float)(nSumOfPixelsIn_NonZeroNorImagesf);
				} //
				else
				{
					fRatioOfSumsOfPixelsOfNonZero_MalToNorf = (float)(nSumOfPixelsIn_NonZeroMalImagesf);
				} //
			} //
////////////////////////////////////////////
			if (fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf > fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf)
			{
				fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf = fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf;

				printf( "\n\n A new fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf = %E, fRatioOfSumsOfPixelsOfNonZero_MalToNorf = %E, nNumOfNonZeroBinsIn_NorImagesf = %d, nNumOfNonZeroBinsIn_MalImagesf = %d, iBinOfAllColorsf = %d",
					fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf, fRatioOfSumsOfPixelsOfNonZero_MalToNorf,nNumOfNonZeroBinsIn_NorImagesf, nNumOfNonZeroBinsIn_MalImagesf, iBinOfAllColorsf);

				fprintf(fout_PrintFeatures, "\n\n A new fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf = %E, fRatioOfSumsOfPixelsOfNonZero_MalToNorf = %E, nNumOfNonZeroBinsIn_NorImagesf = %d, nNumOfNonZeroBinsIn_MalImagesf = %d, iBinOfAllColorsf = %d",
					fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf, fRatioOfSumsOfPixelsOfNonZero_MalToNorf, nNumOfNonZeroBinsIn_NorImagesf, nNumOfNonZeroBinsIn_MalImagesf, iBinOfAllColorsf);

			}//if (fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf > fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMaxf)

			fflush(fout_PrintFeatures);
		} //if (nNumOfNonZeroBinsIn_MalImagesf > 0)

//////////////////////////////////////////
//const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

//const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin,

		if (nNumOfBins_InNonZeroNeg_Imagesf < nNumOfBins_InNonZeroMal_ImagesMax)
		{
			if (nNumOfNonZeroBinsIn_MalImagesf > nNumOf_NonZeroMalImagesMinf)
			{
				if (nSumOfPixelsIn_NonZeroNorImagesf > 0)
				{
					fRatioOfSumsOfPixelsOfNonZero_MalToNorf = (float)(nSumOfPixelsIn_NonZeroMalImagesf) / (float)(nSumOfPixelsIn_NonZeroNorImagesf);

					if (fRatioOfSumsOfPixelsOfNonZero_MalToNorf > fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf)
					{
						if (nNumOfNonZeroBinsIn_NorImagesf > 0) //always works if nSumOfPixelsIn_NonZeroNorImagesf > 0
						{
							fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf = (float)(nNumOfNonZeroBinsIn_MalImagesf) / (float)(nNumOfNonZeroBinsIn_NorImagesf);

							if (fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf > fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf)
							{
								nNumOfBins_InNonZeroNeg_Imagesf += 1;

								nPositOfBins_InNonZeroNeg_Imagesf[nNumOfBins_InNonZeroNeg_Imagesf - 1] = iBinOfAllColorsf;
								goto Mark_Normalization_To_0_1;
							}//if (fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf > fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf)

							else //if (fRatioOfNumOf_NonZeroMalToNorImages_For_ABinf <= fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf)
							{
								goto Mark_Normalization_To_0_1;
							}//else

						} //if (nNumOfNonZeroBinsIn_NorImagesf > 0) //always works if nSumOfPixelsIn_NonZeroNorImagesf > 0

						else //if (nNumOfNonZeroBinsIn_NorImagesf == 0) -- impossible for if (nSumOfPixelsIn_NonZeroNorImagesf > 0)
						{
							nNumOfBins_InNonZeroNeg_Imagesf += 1;

							nPositOfBins_InNonZeroNeg_Imagesf[nNumOfBins_InNonZeroNeg_Imagesf - 1] = iBinOfAllColorsf;
							goto Mark_Normalization_To_0_1;

						}//else //if (nNumOfNonZeroBinsIn_NorImagesf == 0) -- impossible

					} //if (fRatioOfSumsOfPixelsOfNonZero_MalToNorf > fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf)

				}//if (nSumOfPixelsIn_NonZeroNorImagesf > 0)
				else //if (nSumOfPixelsIn_NonZeroNorImagesf == 0)
				{
				//nNumOfNonZeroBinsIn_MalImagesf > nNumOf_NonZeroMalImagesMinf
					nNumOfBins_InNonZeroNeg_Imagesf += 1;

					nPositOfBins_InNonZeroNeg_Imagesf[nNumOfBins_InNonZeroNeg_Imagesf - 1] = iBinOfAllColorsf;
					goto Mark_Normalization_To_0_1;

				} //else //if (nSumOfPixelsIn_NonZeroNorImagesf == 0)

			} //if (nNumOfNonZeroBinsIn_MalImagesf > nNumOf_NonZeroMalImagesMinf)

		} //if (nNumOfBins_InNonZeroNeg_Imagesf < nNumOfBins_InNonZeroMal_ImagesMax)

		fflush(fout_PrintFeatures);
///////////////////////////////////////////////////////

Mark_Normalization_To_0_1:		if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest) //276453)
		{
			fprintf(fout_PrintFeatures, "\n\n Before 'Converting_One_Fea_To_ARange_0_1' at nNumOfBinCur_Glob == nNumOfBinCurOfInterest, iBinOfAllColorsf = %d", iBinOfAllColorsf);

			for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)
			{
				fprintf(fout_PrintFeatures, "\n Before 1, iBinOfAllColorsf = %d: fBinsNor_OneDimf[%d] = %E", iBinOfAllColorsf,iVecf, fBinsNor_OneDimf[iVecf]);
			} //for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)

			fprintf(fout_PrintFeatures, "\n 2:");
			for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
			{
				fprintf(fout_PrintFeatures, "\n Before 2, iBinOfAllColorsf = %d: fBinsMal_OneDimf[%d] = %E", iBinOfAllColorsf,iVecf, fBinsMal_OneDimf[iVecf]);
			} //for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
			fflush(fout_PrintFeatures);

		} //if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest)
///////////////////////////////////////////////

nResf = Converting_One_Fea_To_ARange_0_1(
			nNumOfVecs_Normal, //const int nDim1f,
			nNumOfVecs_Malignant, //const int nDim2f,

			nFeaByRatioTheSameOrNotf, //int &nFeaTheSameOrNotf,
			fFeaByRatioMinf, //float &fFeaMinf,
			fFeaByRatioMaxf, //float &fFeaMaxf,

			fBinsNor_OneDimf, //float fOneDimArr1stf[], //[nDim1f]
			fBinsMal_OneDimf); // float fOneDimArr2ndf[]); //[nDim2f]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			printf("\n\n An error in 'SeparabilityOf_ColorBins' by 'Converting_All_Feas_To_ARange_0_1' 1");
			printf("\n\n Please press any key to exit");

			getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest) //276453)
		{
			fprintf(fout_PrintFeatures, "\n\n After 'Converting_One_Fea_To_ARange_0_1': fFeaByRatioMinf = %E, fFeaByRatioMaxf = %E, iBinOfAllColorsf = %d",
				fFeaByRatioMinf, fFeaByRatioMaxf, iBinOfAllColorsf);

		} //if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest)

///////////////////////////////////

		if (nFeaByRatioTheSameOrNotf == 1)
		{
			if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest) //276453)
			{
				fprintf(fout_PrintFeatures, "\n\n 'nFeaByRatioTheSameOrNotf == 1 in 'SeparabilityOf_ColorBins' at nNumOfBinCur_Glob == nNumOfBinCurOfInterest, iBinOfAllColorsf = %d", 
					iBinOfAllColorsf);
				printf("\n\n 'nFeaByRatioTheSameOrNotf == 1 in 'SeparabilityOf_ColorBins' at nNumOfBinCur_Glob == nNumOfBinCurOfInterest, iBinOfAllColorsf = %d", iBinOfAllColorsf);
				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
				fflush(fout_PrintFeatures);

			} //if (nNumOfBinCur_Glob == 276453)

			continue;
		} //if (nFeaByRatioTheSameOrNotf == 1)
		else
		{
			if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest) //276453)
			{
				fprintf(fout_PrintFeatures, "\n\n Before 'FindingBestSeparByRatioOfOneFea' in 'SeparabilityOf_ColorBins' at nNumOfBinCur_Glob == nNumOfBinCurOfInterest, iBinOfAllColorsf = %d", 
					iBinOfAllColorsf);

				for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)
				{
					fprintf(fout_PrintFeatures, "\n Before 1 (normalized), iBinOfAllColorsf = %d: fBinsNor_OneDimf[%d] = %E", iBinOfAllColorsf, iVecf, fBinsNor_OneDimf[iVecf]);
				} //for (iVecf = 0; iVecf < nNumOfVecs_Normal; iVecf++)

				fprintf(fout_PrintFeatures, "\n 2:");
				for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
				{
					fprintf(fout_PrintFeatures, "\n Before 2 (normalized), iBinOfAllColorsf = %d: fBinsMal_OneDimf[%d] = %E", iBinOfAllColorsf, iVecf, fBinsMal_OneDimf[iVecf]);
				} //for (iVecf = 0; iVecf < nNumOfVecs_Malignant; iVecf++)
				fflush(fout_PrintFeatures);

			} //if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest)
////////////////////////////////

			nResf = FindingBestSeparByRatioOfOneFea(
				fLarge, //const float fLargef,
				nNumOfVecs_Normal, //const int nDim1f,
				nNumOfVecs_Malignant, //const int nDim2f,

				feps, //const  float fepsf,

				fPrecisionOf_G_Const_Search, //const float fPrecisionOf_G_Const_Searchf,

				0.0, //const  float fBorderBelf,
				1.0, //const  float fBorderAbof,

				nNumIterMaxForFindingBestSeparByRatioOfOneFea, //const int nNumIterMaxForFindingBestSeparByRatioOfOneFeaf,

				fBinsNor_OneDimf, //const float fArr1stf[], //0 -- 1
				fBinsMal_OneDimf, //const  float fArr2ndf[], //0 -- 1

				fEfficBestSeparOfOneFeaf, //float &fEfficBestSeparOfOneFeaf,

				fPosSeparOfOneFeaBestf, //float &fPosSeparOfOneFeaBestf,

				nSeparDiffDirectionBestf, //int &nSeparDiffDirectionBestf, //1 -- Abo, -1 -- Bel
				nSeparRatioDirectionBestf, //int &nSeparRatioDirectionBestf, //1 -- Abo, -1 -- Bel

				fNumArr1stSeparBestf, //float &fNumArr1stSeparBestf,
				fNumArr2ndSeparBestf, //float &fNumArr2ndSeparBestf,

				fDiffSeparOfArr1stAndArr2ndBestf, //float &fDiffSeparOfArr1stAndArr2ndBestf,
				fRatioOfSeparOfArr1stAndArr2ndBestf); // float &fRatioOfSeparOfArr1stAndArr2ndBestf)

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				printf("\n\n An error in 'SeparabilityOf_ColorBins' by 'FindingBestSeparByRatioOfOneFea', iBinOfAllColorsf = %d", iBinOfAllColorsf);
				printf("\n\n Please press any key to exit");

				getchar(); exit(1);

				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			if (fEfficBestSeparOfOneFeaf < fRatioBest_Minf)
			{
				fRatioBest_Minf = fEfficBestSeparOfOneFeaf;

				printf("\n\n 'SeparabilityOf_ColorBins': a new fRatioBest_Minf = %E, iBinOfAllColorsf = %d",
					fRatioBest_Minf, iBinOfAllColorsf);

			//	printf("\n fPosSeparOfOneFeaBestf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E, nSeparRatioDirectionBestf = %d",
				//	fPosSeparOfOneFeaBestf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf, nSeparRatioDirectionBestf);


				fprintf(fout_PrintFeatures, "\n\n 'SeparabilityOf_ColorBins': a new fRatioBest_Minf = %E, iBinOfAllColorsf = %d",
					fRatioBest_Minf, iBinOfAllColorsf);

				fprintf(fout_PrintFeatures, "\n fPosSeparOfOneFeaBestf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E, nSeparRatioDirectionBestf = %d",
					fPosSeparOfOneFeaBestf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf, nSeparRatioDirectionBestf);

				nResf = Converting_nBinOfAllColorsf_To_SeparateColors(
					iBinOfAllColorsf, //const int nBinOfAllColorsf,
					nRedBinf, //int &nRedBinf,
					nGreenBinf, //int &nGreenBinf,
					nBlueBinf); // int &nBlueBinf)

				//printf("\n\n nRedBinf = %d, nGreenBinf = %d, nBlueBinf = %d", nRedBinf, nGreenBinf, nBlueBinf);
				printf("\n\n The color intensity intervals: for Red from %d to %d, for Green from %d to %d, for Blue from %d to %d", 
					nRedBinf*nBinSizeForColorSepar, (nRedBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar,

					nGreenBinf*nBinSizeForColorSepar, (nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar,

					nBlueBinf*nBinSizeForColorSepar, (nBlueBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar);

				fprintf(fout_PrintFeatures, "\n\n nRedBinf = %d, nGreenBinf = %d, nBlueBinf = %d",	nRedBinf, nGreenBinf, nBlueBinf);

				fprintf(fout_PrintFeatures,"\n\n The color intensity intervals: for Red from %d to %d, for Green from %d to %d, for Blue from %d to %d",
					nRedBinf*nBinSizeForColorSepar, (nRedBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar,

					nGreenBinf*nBinSizeForColorSepar, (nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar,

					nBlueBinf*nBinSizeForColorSepar, (nBlueBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar);

				if ((nRedBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
				{
					printf("\n\n An error in 'SeparabilityOf_ColorBins': (nRedBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar = %d, iBinOfAllColorsf = %d",
						(nRedBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar, iBinOfAllColorsf);

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if ((nRedBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)

				if ((nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
				{
					printf("\n\n An error in 'SeparabilityOf_ColorBins': (nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar = %d, iBinOfAllColorsf = %d",
						(nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar, iBinOfAllColorsf);

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if ((nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)

				if ((nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
				{
					printf("\n\n An error in 'SeparabilityOf_ColorBins': (nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar = %d, iBinOfAllColorsf = %d",
						(nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar, iBinOfAllColorsf);

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if ((nGreenBinf*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
//////////////////////////////////////////////////
				fBestBinSeparabilityf = fRatioBest_Minf;

				printf("\n\n /////////////////////////////////////////////////");
				fflush(fout_PrintFeatures);
			} // if (fEfficBestSeparOfOneFeaf < fRatioBest_Minf)

			if (fEfficBestSeparOfOneFeaf < fEfficiency_ToPrint)
			{
				fprintf(fout_PrintFeatures, "\n\n 'SeparabilityOf_ColorBins': a next fRatioBest_Minf = %E < fEfficiency_ToPrint = %E, iBinOfAllColorsf = %d",
					fRatioBest_Minf, fEfficiency_ToPrint, iBinOfAllColorsf);

				fprintf(fout_PrintFeatures, "\n fPosSeparOfOneFeaBestf = %E, fNumArr1stSeparBestf = %E, fNumArr2ndSeparBestf = %E, nSeparRatioDirectionBestf = %d",
					fPosSeparOfOneFeaBestf, fNumArr1stSeparBestf, fNumArr2ndSeparBestf, nSeparRatioDirectionBestf);

				fflush(fout_PrintFeatures);
			} // if (fEfficBestSeparOfOneFeaf < fEfficiency_ToPrint)

			if ((iBinOfAllColorsf / 50000) * 50000 == iBinOfAllColorsf)
			{
				//printf("\n\n So far fRatioBest_Minf = %E, iBinOfAllColorsf = %d, current fEfficBestSeparOfOneFeaf = %E",	fRatioBest_Minf, iBinOfAllColorsf, fEfficBestSeparOfOneFeaf);

			} //if ((iBinOfAllColorsf / 100) * 100 == iBinOfAllColorsf)

		} //else

		if (nNumOfBinCur_Glob == nNumOfBinCurOfInterest) //276453)
		{
			printf("\n\n The end in 'SeparabilityOf_ColorBins' at nNumOfBinCur_Glob == nNumOfBinCurOfInterest, fEfficBestSeparOfOneFeaf = %E, iBinOfAllColorsf = %d",
				fEfficBestSeparOfOneFeaf, iBinOfAllColorsf);

			fprintf(fout_PrintFeatures, "\n\n The end in 'SeparabilityOf_ColorBins' at nNumOfBinCur_Glob == nNumOfBinCurOfInterest, fEfficBestSeparOfOneFeaf = %E, iBinOfAllColorsf = %d",
				fEfficBestSeparOfOneFeaf, iBinOfAllColorsf);

			//printf("\n\n Please press any key to exit"); getchar(); exit(1);
			fflush(fout_PrintFeatures);
		} //if (nNumOfBinCur_Glob == 276453)

	}//for (iBinOfAllColorsf = 0; iBinOfAllColorsf < nNumOfBins_OneImage_AllColorsf; iBinOfAllColorsf++)

	return SUCCESSFUL_RETURN;
}// int SeparabilityOf_ColorBins(...
/////////////////////////////////////////////

int Converting_nBinOfAllColorsf_To_SeparateColors(
	const int nBinOfAllColorsf,
	int &nRedBinf,
	int &nGreenBinf,
	int &nBlueBinf)
{
/*
			nIndexOfBinCurf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) +( nBlueBinf * (nNumOfBins_OneColor* nNumOfBins_OneColor));

*/
	int
		nIndexOfBin_Verifiedf,

		nResidualf;

	if (nBinOfAllColorsf < 0 || nBinOfAllColorsf >= nNumOfBins_OneImage_AllColors)
	{
		printf("\n\n An error in 'Converting_nBinOfAllColorsf_To_SeparateColors': nBinOfAllColorsf = %d >= nNumOfBins_OneImage_AllColors = %d",
			nBinOfAllColorsf, nNumOfBins_OneImage_AllColors);
		printf("\n\n Please press any key to exit");			getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nBinOfAllColorsf < 0 || nBinOfAllColorsf >= nNumOfBins_OneImage_AllColors)

	nBlueBinf = nBinOfAllColorsf / (nNumOfBins_OneColor*nNumOfBins_OneColor); // nSquareOfNumOfBins_OneColor;

	nResidualf = nBinOfAllColorsf - (nBlueBinf*nNumOfBins_OneColor*nNumOfBins_OneColor); //nSquareOfNumOfBins_OneColor);

	nGreenBinf = nResidualf / nNumOfBins_OneColor;

	nRedBinf = nResidualf - (nGreenBinf*nNumOfBins_OneColor);

	nIndexOfBin_Verifiedf = nRedBinf + (nGreenBinf* nNumOfBins_OneColor) + (nBlueBinf * nNumOfBins_OneColor*nNumOfBins_OneColor); //nSquareOfNumOfBins_OneColor);

	if (nIndexOfBin_Verifiedf != nBinOfAllColorsf)
	{
		printf("\n\n An error in 'Converting_nBinOfAllColorsf_To_SeparateColors': nBinOfAllColorsf = %d != nIndexOfBin_Verifiedf = %d",
			nBinOfAllColorsf, nIndexOfBin_Verifiedf);
		printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	}//if (nIndexOfBin_Verifiedf != nBinOfAllColorsf)

	return SUCCESSFUL_RETURN;
} //int Converting_nBinOfAllColorsf_To_SeparateColors(...


////////////////////////////////////////////////

/*
//From Wikipedia, https://en.wikipedia.org/wiki/HSL_and_HSV
//From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
// Alternative: Practical algorithms for image analysis, Michael Seul and others, p.53

int HLS_Fr_RGB(
	const COLOR_IMAGE *sColor_Imagef, //[nImageAreaMax]

	HLS_IMAGE *sHLS_Imagef)
*/

///////////////////////////////////////////////////////////////////////////////
  //printf("\n\n Please press any key:"); getchar();



