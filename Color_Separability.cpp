
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

int
	nNumOfProcessedFiles_NormalTot_Glob;
//#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

//#endif //#ifdef GABOR_FILTER_IN_ONE_DIRECTION_APPLIED

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
#include "Color_Separability_function.cpp"

int main()
{
	int Reading_AColorImage(
		const Image& image_in,

		COLOR_IMAGE *sColor_Image);

	int	Converting_OneFeaVec_ToAVecWithSelecFeas(
		const int nDim_1f,
		const int nDim_2f, // == nNumOfFeasWithMeaningfulValues_Finf < nDim_1f

		const float f_1Arrf[], //[nDim_1f]

		const int nFeaTheSameOrNot_FinArrf[], //[nDim_1f]

		float f_SelecArrf[]); //[nDim_2f] //fActualFeasFor_OneNormalVec_CoocExtendedArr[]

#ifdef USING_HISTOGRAM_EQUALIZATION
	int HistogramEqualization_ForColorFormatOfGray(

		//GRAYSCALE_IMAGE *sGrayscale_Image)
		COLOR_IMAGE *sColor_Imagef);

	int Histogram_Statistics_ColorActuallyGrayscale_Image(

		//const GRAYSCALE_IMAGE *sGrayscale_Imagef, //[nImageAreaMax]
		const COLOR_IMAGE *sColor_Imagef,

		float fPercentsOfHistogramIntervalsArrf[],//[nNumOfHistogramIntervals_ForCooc]

		float fPercentagesAboveThresholds_InHistogramArrf[], //[nNumOfHistogramIntervals_ForCooc - 2]

		float &fMeanf,
		float &fStDevf,
		float &fSkewnessf,
		float &fKurtosisf);

#endif //#ifdef USING_HISTOGRAM_EQUALIZATION

	void Initializing_AFloatVec_To_Zero(
		const int nDimf,
		float fVecArrf[]); //[nDimf]

	int NumOfPixelsIn_ColorBins(
		const int nImageWidthf,
		const int nImageLengthf,

		const COLOR_IMAGE *sColor_Imagef, //[]

		int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
		int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
		int nNumOfPixelsIn_BlueBinsArrf[], //[nNumOfBins_OneColor]
		///////////////////////////////////////////////////
		int nNumOfPixelsIn_CombinedColorBinsArrf[]); //[nNumOfBins_OneImage_AllColors]

	int SeparabilityOf_ColorBins(

		const int nNumOfBins_OneImage_AllColorsf,
////////////////////////////////////
//for NonZero images
		const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

		const int nNumOf_NonZeroMalImagesMinf,

		const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

		const float fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf,

///////////////////////////////

		const int nNumOfVecs_Norf,
		const int nNumOfVecs_Malf,

		const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

		const float fAllBinsAll_ColorsAver_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
		const float fAllBinsAll_ColorsAver_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
	/////////////////////////////////////////////
		float &fBestBinSeparabilityf,
		int &nNumOfBins_InNonZeroNeg_Imagesf,

		int nPositOfBins_InNonZeroNeg_Imagesf[], //[nNumOfBins_InNonZeroMal_ImagesMax] 

		float &fLargestRatioOfBinsAboveTheMinRatioForSeparf);

	int Converting_nBinOfAllColorsf_To_SeparateColors(
		const int nBinOfAllColorsf,
		int &nRedBinf,
		int &nGreenBinf,
		int &nBlueBinf);
//////////////////////////////////////////////////////
	//printf("\n\n 1_1: Press any key:");	getchar();

	float
		fNumOfBins_AllColorsForAll_NorImages = (float)(nNumOfBins_OneImage_AllColors)*(float)(nNumOfVecs_Normal),
		fNumOfBins_AllColorsForAll_MalImages = (float)(nNumOfBins_OneImage_AllColors)*(float)(nNumOfVecs_Malignant);

	if (fNumOfBins_AllColorsForAll_NorImages > 2.0E+9 || fNumOfBins_AllColorsForAll_MalImages > 2.0E+9)
	{
		printf("\n\n An error by too may feas in 'main': fNumOfBins_AllColorsForAll_NorImages = %E, fNumOfBins_AllColorsForAll_MalImages = %E",
			fNumOfBins_AllColorsForAll_NorImages, fNumOfBins_AllColorsForAll_MalImages);

		printf("\n\n Please increase 'nBinSizeForColorSepar' or decrease the number of images");
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (fNumOfBins_AllColorsForAll_NorImages > 2.0E+9 || fNumOfBins_AllColorsForAll_MalImages > 2.0E+9)
////////////////////////////////////////////////////////
	
/*printf("\n\n A try:");	

	int
		iBin,
		nIndexOfBin;

	for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
	{
		//nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;
		nIndexOfBin = iBin; // +(nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;

		//if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)
		if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfBins_AllColorsForAll_NorImages)
		{
			printf("\n\n An error in 'main' 2: nIndexOfBin = %d >= nNumOfBins_AllColorsForAll_NorImages = %d, iBin = %d, nNumOfBins_OneImage_AllColors = %d",
				nIndexOfBin, nNumOfBins_AllColorsForAll_NorImages, iBin, nNumOfBins_OneImage_AllColors);

			printf("\n\n Press any key to exit");	getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)

		//nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBinsArr[iBin];

		//fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBinsArr[iBin]) / (float)(nImageAreaCur);

		//printf("\n iBin = %d, nAllBinsAll_Colors_NorImagesArr[%d] = %d, fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = %E",
			//iBin,nIndexOfBin, nAllBinsAll_Colors_NorImagesArr[nIndexOfBin], fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin]);
	} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)

	printf("\n\n 5_2: please press any key:");	getchar();
*/

#if 1
	int
		iFea,

		nImageWidth, // = image_in.width();
		nImageHeight, // = image_in.height();
		nSizeOfImage,

		nIntensity_Read_Test_ImageMax = -nLarge,

		nIndexOfPixelCur,

		nNumOfBins_InNonZeroNeg_Images,
		nRed, 
		nGreen,
		nBlue,
		nRedBin, //int &nRedBinf,
		nGreenBin, //int &nGreenBinf,
		nBlueBin, // int &nBlueBinf)

		nIndex,

		nIndex_AllVecs,
		nInitFea_ForAllVecs,
		nTempf,

		nDimSelec,
		nNumOfProcessedFiles_NormalTot = 0,
		nNumOfProcessedFiles_MalignantTot = 0,

		nNumOf_ActualFeas_ForAll_Normal_VecsTotf,
		nNumOf_ActualFeas_ForAll_Malignant_VecsTotf,

		////////////////////////

		nNumOfPixelsIn_RedBinsArr[nNumOfBins_OneColor],
		nNumOfPixelsIn_GreenBinsArr[nNumOfBins_OneColor],
		nNumOfPixelsIn_BlueBinsArr[nNumOfBins_OneColor],

		nNumOfPixelsIn_CombinedColorBinsArr[nNumOfBins_OneImage_AllColors], //

//#define nNumOfOneColorBinsInAll_NorImages (nNumOfVecs_Normal * nNumOfBins_OneColor)
//#define nNumOfOneColorBinsInAll_MalImages (nNumOfVecs_Malignant * nNumOfBins_OneColor)

		nRedAll_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		nGreenAll_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		nBlueAll_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],

		nRedAll_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		nGreenAll_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		nBlueAll_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
///////////////////////
		nAllBinsAll_Colors_NorImagesArr[nNumOfBins_AllColorsForAll_NorImages],
		nAllBinsAll_Colors_MalImagesArr[nNumOfBins_AllColorsForAll_MalImages],

		nPositOfBins_InNonZeroNeg_Images[nNumOfBins_InNonZeroMal_ImagesMax], //[nNumOfBins_InNonZeroMal_ImagesMax] 

		nImageAreaCur,

		iBin,
		nIndexOfBin,

		nPositOfBins_InNonZeroNegCur,
///////////////////////
		iVec,
		nRes;
//////////////////////////////////
//	printf("\n\n 1_2: Press any key:");	getchar();

	float
		fFeaCur,
		////////////////////////////////////////

		fRedAllAver_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		fGreenAllAver_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],
		fBlueAllAver_NorImagesArr[nNumOfOneColorBinsInAll_NorImages],

		fRedAllAver_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		fGreenAllAver_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		fBlueAllAver_MalImagesArr[nNumOfOneColorBinsInAll_MalImages],
		////////////////////////////////		
		fAllBinsAll_ColorsAver_NorImagesArr[nNumOfBins_AllColorsForAll_NorImages],
		fAllBinsAll_ColorsAver_MalImagesArr[nNumOfBins_AllColorsForAll_MalImages],

		fBestBinSeparability,

		fLargestRatioOfBinsAboveTheMinRatioForSepar,

		fNumOf_NormalVecsSeparBestMax,
		fNumOf_MalignantVecsSeparBestMax;

	////////////////////////////////////////////////////////////////
	
	//printf("\n\n 1: Press any key:");	getchar();
	//		nNumOfPixelsIn_CombinedColorBinsArr[nNumOfBins_OneImage_AllColors], //
	for (iFea = 0; iFea < nNumOfBins_OneImage_AllColors; iFea++)
	{
		//[nNumOfBins_OneImage_AllColors]
		nNumOfPixelsIn_CombinedColorBinsArr[iFea] = 0;
	} // for (iFea = 0; iFea < nNumOfBins_OneImage_AllColors; iFea++)


	for (iFea = 0; iFea < nNumOfOneColorBinsInAll_NorImages; iFea++)
	{
		//nNumOfOneColorBinsInAll_NorImages
		nRedAll_NorImagesArr[iFea] = 0;
		nGreenAll_NorImagesArr[iFea] = 0;
		nBlueAll_NorImagesArr[iFea] = 0;

		//nNumOfOneColorBinsInAll_NorImages
		fRedAllAver_NorImagesArr[iFea] = 0.0;
		fGreenAllAver_NorImagesArr[iFea] = 0.0;
		fBlueAllAver_NorImagesArr[iFea] = 0.0;

	} // for (iFea = 0; iFea < nNumOfOneColorBinsInAll_NorImages; iFea++)
////////////////////////////////
//	printf("\n\n 2: Press any key:");	getchar();

	for (iFea = 0; iFea < nNumOfOneColorBinsInAll_MalImages; iFea++)
	{
		//nNumOfOneColorBinsInAll_MalImages
		nRedAll_MalImagesArr[iFea] = 0;
		nGreenAll_MalImagesArr[iFea] = 0;
		nBlueAll_MalImagesArr[iFea] = 0;

		//nNumOfOneColorBinsInAll_MalImages
		fRedAllAver_MalImagesArr[iFea] = 0.0;
		fGreenAllAver_MalImagesArr[iFea] = 0.0;
		fBlueAllAver_MalImagesArr[iFea] = 0.0;
	} // for (iFea = 0; iFea < nNumOfOneColorBinsInAll_MalImages; iFea++)
//////////////////////////////
	//printf("\n\n 3: Press any key:");	getchar();

	for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
	{
		//nNumOfBins_AllColorsForAll_NorImages
		nAllBinsAll_Colors_NorImagesArr[iFea] = 0;

		//nNumOfBins_AllColorsForAll_NorImages
		fAllBinsAll_ColorsAver_NorImagesArr[iFea] = 0.0;
	} // for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_NorImages; iFea++)
//////////////////////////////

	for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_MalImages; iFea++)
	{
		//nNumOfBins_AllColorsForAll_MalImages
		nAllBinsAll_Colors_MalImagesArr[iFea] = 0;

		//nNumOfBins_AllColorsForAll_MalImages
		fAllBinsAll_ColorsAver_MalImagesArr[iFea] = 0.0;
	} // for (iFea = 0; iFea < nNumOfBins_AllColorsForAll_MalImages; iFea++)

	//printf("\n\n 4: Press any key:");	getchar();

////////////////////////////////////////////////////////////
//https://docs.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=vs-2019
	//printf("\n\n LONG_MAX = %lld", (long long)(LONG_MAX)); printf("\n\n Please press any key to exit"); getchar(); exit(1);
	//printf("\n\n (float)(LLONG_MAX) = %E", (float)(LLONG_MAX)); printf("\n\n Please press any key to exit"); getchar(); exit(1);

	//////////////////////////////////////////////////////////////
	//fout_PrintFeatures = fopen("wColorBins_Spleen_50_50.txt", "w");
fout_PrintFeatures = fopen("wColorBins_WoodyBreast_500_298_Symm_3_16Bin.txt", "w");
	
	//fout_PrintFeatures = fopen("wColorBins_Spleen_227_311.txt", "w");

	//fout_PrintFeatures = fopen("wVecsSpleen_Ben_Mal_472_575_Cooc2.txt", "w");

	if (fout_PrintFeatures == NULL)
	{
		printf("\n\n fout_PrintFeatures == NULL");
		getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_PrintFeatures == NULL)
/////////////////////////////
	printf("\n\n The beginning: fNumOfBins_AllColorsForAll_NorImages = %E, fNumOfBins_AllColorsForAll_MalImages = %E, nNumOfBins_OneImage_AllColors = %d",
		fNumOfBins_AllColorsForAll_NorImages, fNumOfBins_AllColorsForAll_MalImages, nNumOfBins_OneImage_AllColors);
	fprintf(fout_PrintFeatures, "\n\n The beginning: fNumOfBins_AllColorsForAll_NorImages = %E, fNumOfBins_AllColorsForAll_MalImages = %E, nNumOfBins_OneImage_AllColors = %d",
		fNumOfBins_AllColorsForAll_NorImages, fNumOfBins_AllColorsForAll_MalImages, nNumOfBins_OneImage_AllColors);

	//printf("\n\n Please press any key:");	getchar();

	////////////////////
	
	Image image_in;

	char cFileName_iinDirectoryOf_Normal_Images[200]; //[150]
	char cFileName_iinDirectoryOf_Malignant_Images[200]; //[150]
	//image_in.read("output_ForMultifractal.png");
/////////////////////////////////////////////////////////////
	DIR *pDIR;
	struct dirent *entry;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//woody breast
//50 D:\Imago\Images\spleen\Benign_AOI_50_SelecColor
	//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\spleen\\Benign_AOI_50_SelecColor\\"; //copy to if (pDIR = opendir(.. as well
	
	//500
//D:\Imago\Images\WoodyBreast\Color_Images\Oct20_Symmetry_2\Normal-COBB-AOI
//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\Normal-COBB-AOI\\"; //copy to if (pDIR = opendir(.. as well

//D:\Imago\Images\WoodyBreast\Color_Images\Oct27_Sym_3\COBB-Normal-Sym3
char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-Normal-Sym3\\"; //copy to if (pDIR = opendir(.. as well

	//227
	//char cInput_DirectoryOf_Normal_Images[200] = "D:\\Imago\\Images\\spleen\\Color\\BenignAOI-Color-PNG\\"; //copy to if (pDIR = opendir(.. as well

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//check out '#define nNumOfVecs_Malignant'!
//50 D:\Imago\Images\spleen\Malignant_AOI_50_SelecColor
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\";

//298 //D:\Imago\Images\WoodyBreast\Color_Images\Oct20_Symmetry_2\WB-Color-AOI
//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\WB-Color-AOI\\";

//D:\Imago\Images\WoodyBreast\Color_Images\Oct27_Sym_3\COBB-WB-Sym3
char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-WB-Sym3\\";

//311
	//char cInput_DirectoryOf_Malignant_Images[200] = "D:\\Imago\\Images\\spleen\\Color\\MalignantAOI-Color-PNG\\"; //copy to if (pDIR = opendir(.. as well

//////////////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

/////////////////////////////////////////////////////
//Normal 
	//////////////////////////////////////////////////////////////

	nNumOfProcessedFiles_NormalTot = 0;
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Benign_AOI_50_SelecColor\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\Normal-COBB-AOI\\"))

	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-Normal-Sym3\\"))


	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Color\\BenignAOI-Color-PNG\\"))
	{
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_NormalTot += 1;

				nNumOfProcessedFiles_NormalTot_Glob = nNumOfProcessedFiles_NormalTot;

				if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_NormalTot = %d > nNumOfVecs_Normal = %d", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Please press any key to exit"); fflush(fout_PrintFeatures);  getchar(); exit(1);
			
					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_NormalTot > nNumOfVecs_Normal)

//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_iinDirectoryOf_Normal_Images, '\0', sizeof(cFileName_iinDirectoryOf_Normal_Images));

				strcpy(cFileName_iinDirectoryOf_Normal_Images, cInput_DirectoryOf_Normal_Images);

				strcat(cFileName_iinDirectoryOf_Normal_Images, entry->d_name);

				fprintf(fout_PrintFeatures,"\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);

				if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
				{
					printf("\n Concatenated input file: %s, nNumOfProcessedFiles_NormalTot = %d\n", cFileName_iinDirectoryOf_Normal_Images, nNumOfProcessedFiles_NormalTot);
				} //if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
////////////////////////////////////////
				image_in.read(cFileName_iinDirectoryOf_Normal_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_PrintFeatures, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Normal_Images);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n Press any key 1:");	fflush(fout_lr);  getchar();
//////////////////////////////////////////////////////////////

				nImageAreaCur = 5000; //test only

				nRes = Reading_AColorImage(
					image_in, //const Image& image_in,

					&sColor_Image); // COLOR_IMAGE *sColor_Image);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Reading_AColorImage' 1");

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
				{
					printf("\n\n So far nNumOfProcessedFiles_NormalTot = %d", nNumOfProcessedFiles_NormalTot);
				} //if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)

				nImageAreaCur = sColor_Image.nLength*sColor_Image.nWidth;

				if (nImageAreaCur < nImageAreaMin)
				{
					printf("\n\n An error in 'main' 1: nImageAreaCur = %d < nImageAreaMin = %d", nImageAreaCur,nImageAreaMin);

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if (nImageAreaCur < nImageAreaMin)

////////////////////////////////////

				nRes = NumOfPixelsIn_ColorBins(
					sColor_Image.nWidth, //const int nImageWidthf,
					sColor_Image.nLength, //const int nImageLengthf,

					&sColor_Image, //COLOR_IMAGE *sColor_Imagef, //[]

					nNumOfPixelsIn_RedBinsArr, //int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_GreenBinsArr, //int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_BlueBinsArr, // int nNumOfPixelsIn_BlueBinsArrf[]) //[nNumOfBins_OneColor]

					nNumOfPixelsIn_CombinedColorBinsArr); // int nNumOfPixelsIn_CombinedColorBinsArrf[]); //[nNumOfBins_OneImage_AllColors]

				for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1) * nNumOfBins_OneColor;
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)
					{
						printf("\n\n An error in 'main' 1: nIndexOfBin = %d >= nNumOfOneColorBinsInAll_NorImages = %d", nIndexOfBin, nNumOfOneColorBinsInAll_NorImages);

						printf("\n\n Press any key to exit");	getchar(); exit(1);
						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)

					nRedAll_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_RedBinsArr[iBin];
					nGreenAll_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_GreenBinsArr[iBin];
					nBlueAll_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_BlueBinsArr[iBin];

					fRedAllAver_NorImagesArr[nIndexOfBin] = (float)(nRedAll_NorImagesArr[nIndexOfBin])/(float)(nImageAreaCur);
					fGreenAllAver_NorImagesArr[nIndexOfBin] = (float)(nGreenAll_NorImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
					fBlueAllAver_NorImagesArr[nIndexOfBin] = (float)(nBlueAll_NorImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
				} //for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)

///////////////////////////////////////////
				//printf("\n\n 5_1: please press any key:");	getchar();

//fAllBinsAll_ColorsAver_NorImagesArr[nNumOfBins_AllColorsForAll_NorImages],
				for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_NormalTot - 1)* nNumOfBins_OneImage_AllColors;

					//if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfBins_AllColorsForAll_NorImages)
					{
						printf("\n\n An error in 'main' 2: nIndexOfBin = %d >= nNumOfBins_AllColorsForAll_NorImages = %d, iBin = %d, nNumOfBins_OneImage_AllColors = %d",
							nIndexOfBin, nNumOfBins_AllColorsForAll_NorImages, iBin, nNumOfBins_OneImage_AllColors);

						printf("\n\n Press any key to exit");	getchar(); exit(1);
						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_NorImages)

					nAllBinsAll_Colors_NorImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBinsArr[iBin];

					fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBinsArr[iBin]) / (float)(nImageAreaCur);

					//printf("\n iBin = %d, nAllBinsAll_Colors_NorImagesArr[%d] = %d, fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin] = %E",
						//iBin,nIndexOfBin, nAllBinsAll_Colors_NorImagesArr[nIndexOfBin], fAllBinsAll_ColorsAver_NorImagesArr[nIndexOfBin]);

				} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)

			//	printf("\n\n 5_2: please press any key:");	getchar();

		////////////////////////////////////////////////

				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)
				{
					printf("\n Normal: the end of nNumOfProcessedFiles_NormalTot = %d, nNumOfVecs_Normal = %d; going to to the next image", nNumOfProcessedFiles_NormalTot, nNumOfVecs_Normal);
				} //if ((nNumOfProcessedFiles_NormalTot / 100) * 100 == nNumOfProcessedFiles_NormalTot)

				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Normal_AOI_50_SelecColor\\"))

	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n An error: the directory of normal images can not be opened");

		printf("\n\n Press any key to exit"); fflush(fout_PrintFeatures);	getchar(); exit(1);

		perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else

//printf("\n\n The end of normal inages: press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);
	//printf("\n\n The end of normal images: press any key to continue to the malignant ones");	fflush(fout_PrintFeatures);  getchar();

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//Malignant 

//check out '#define nNumOfVecs_Malignant'!
	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\"))
	//if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct20_Symmetry_2\\WB-Color-AOI\\"))

	if (pDIR = opendir("D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Oct27_Sym_3\\COBB-WB-Sym3\\"))

	//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Color\\MalignantAOI-Color-PNG\\"))
	{
		nNumOfProcessedFiles_MalignantTot = 0;
		while (entry = readdir(pDIR))
		{
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
			{
				nNumOfProcessedFiles_MalignantTot += 1;

				if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)
				{
					printf("\n\n An error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n n error in counting the number of normal images: nNumOfProcessedFiles_MalignantTot = %d > nNumOfVecs_Malignant = %d", nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					printf("\n\n Press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nNumOfProcessedFiles_MalignantTot > nNumOfVecs_Malignant)

				//Initializing_AFloatVec_To_Zero(
					//nNumOf_CoocExtendedFeas_OneDimTot, //const int nDimf,
					//fOneDim_CoocExtendedArr); // float fVecArrf[]); //[nDimf]

//#ifndef COMMENT_OUT_ALL_PRINTS
				//printf("\n\n %s\n", entry->d_name);
				//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				memset(cFileName_iinDirectoryOf_Malignant_Images, '\0', sizeof(cFileName_iinDirectoryOf_Malignant_Images));

				strcpy(cFileName_iinDirectoryOf_Malignant_Images, cInput_DirectoryOf_Malignant_Images);

				strcat(cFileName_iinDirectoryOf_Malignant_Images, entry->d_name);

				if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)
				{
					printf("\n Concatenated input file: %s, nNumOfProcessedFiles_MalignantTot = %d\n", cFileName_iinDirectoryOf_Malignant_Images, nNumOfProcessedFiles_MalignantTot);
				} //if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)

				//fprintf(fout_PrintFeatures, "\n\n Concatenated input file: %s\n", cFileName_iinDirectoryOf_Malignant_Images);

				//printf("\n\n Press any key 1:");	fflush(fout_PrintFeatures);  getchar();
////////////////////////////////////////////////
				image_in.read(cFileName_iinDirectoryOf_Malignant_Images);

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n After reading the input image");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//#ifndef COMMENT_OUT_ALL_PRINTS

				nRes = Reading_AColorImage(
					image_in, //const Image& image_in,

					&sColor_Image); // COLOR_IMAGE *sColor_Image);

				if (nRes == UNSUCCESSFUL_RETURN)
				{
					printf("\n\n An error in 'main' for 'Reading_AColorImage' 2");

					printf("\n\n Press any key to exit");	getchar(); exit(1);

					return UNSUCCESSFUL_RETURN;
				} // if (nRes == UNSUCCESSFUL_RETURN)

				if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)
				{
					printf("\n\n So far nNumOfProcessedFiles_MalignantTot = %d", nNumOfProcessedFiles_MalignantTot);
				} //if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)

/////////////////////////////////////////////////////////////////

				nImageAreaCur = sColor_Image.nLength*sColor_Image.nWidth;

				if (nImageAreaCur < nImageAreaMin)
				{
					printf("\n\n An error in 'main' 2: nImageAreaCur = %d < nImageAreaMin = %d", nImageAreaCur, nImageAreaMin);

					printf("\n\n Press any key to exit");	getchar(); exit(1);
					return UNSUCCESSFUL_RETURN;
				} //if (nImageAreaCur < nImageAreaMin)
////////////////////////////////////

				nRes = NumOfPixelsIn_ColorBins(
					sColor_Image.nWidth, //const int nImageWidthf,
					sColor_Image.nLength, //const int nImageLengthf,

					&sColor_Image, //COLOR_IMAGE *sColor_Imagef, //[]

					nNumOfPixelsIn_RedBinsArr, //int nNumOfPixelsIn_RedBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_GreenBinsArr, //int nNumOfPixelsIn_GreenBinsArrf[], //[nNumOfBins_OneColor]
					nNumOfPixelsIn_BlueBinsArr, // int nNumOfPixelsIn_BlueBinsArrf[]) //[nNumOfBins_OneColor]

					nNumOfPixelsIn_CombinedColorBinsArr); // int nNumOfPixelsIn_CombinedColorBinsArrf[]); //[nNumOfBins_OneImage_AllColors]


				for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_MalignantTot - 1)* nNumOfBins_OneColor;
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)
					{
						printf("\n\n An error in 'main' 3: nIndexOfBin = %d >= nNumOfOneColorBinsInAll_MalImages = %d", nIndexOfBin, nNumOfOneColorBinsInAll_MalImages);

						printf("\n\n Press any key to exit");	getchar(); exit(1);
						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)

					nRedAll_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_RedBinsArr[iBin];
					nGreenAll_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_GreenBinsArr[iBin];
					nBlueAll_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_BlueBinsArr[iBin];

					fRedAllAver_MalImagesArr[nIndexOfBin] = (float)(nRedAll_MalImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
					fGreenAllAver_MalImagesArr[nIndexOfBin] = (float)(nGreenAll_MalImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
					fBlueAllAver_MalImagesArr[nIndexOfBin] = (float)(nBlueAll_MalImagesArr[nIndexOfBin]) / (float)(nImageAreaCur);
				} //for (iBin = 0; iBin < nNumOfBins_OneColor; iBin++)
///////////////////////////////////////////

//fAllBinsAll_ColorsAver_MalImagesArr[nNumOfBins_AllColorsForAll_MalImages],
				for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
				{
					nIndexOfBin = iBin + (nNumOfProcessedFiles_MalignantTot - 1)* nNumOfBins_OneImage_AllColors;

					//if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)
					if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfBins_AllColorsForAll_MalImages)
					{
						printf("\n\n An error in 'main' 4: nIndexOfBin = %d >= nNumOfBins_AllColorsForAll_MalImages = %d", nIndexOfBin, nNumOfBins_AllColorsForAll_MalImages);

						printf("\n\n Press any key to exit");	getchar(); exit(1);
						return UNSUCCESSFUL_RETURN;
					} //if (nIndexOfBin < 0 || nIndexOfBin >= nNumOfOneColorBinsInAll_MalImages)

					nAllBinsAll_Colors_MalImagesArr[nIndexOfBin] = nNumOfPixelsIn_CombinedColorBinsArr[iBin];

					fAllBinsAll_ColorsAver_MalImagesArr[nIndexOfBin] = (float)(nNumOfPixelsIn_CombinedColorBinsArr[iBin]) / (float)(nImageAreaCur);
				} //for (iBin = 0; iBin < nNumOfBins_OneImage_AllColors; iBin++)
////////////////////////////////////////////////////////////////////
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				//printf("\n\n The end of 'Copying_CoocFeaturesAsAVector', nNumOfProcessedFiles_NormalTot = %d, please press any key to switch to the next image", nNumOfProcessedFiles_NormalTot);  getchar(); //exit(1);
				if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)
				{
					printf("\n Malignant: the end of nNumOfProcessedFiles_MalignantTot = %d, nNumOfVecs_Malignant = %d; going to to the next image",
						nNumOfProcessedFiles_MalignantTot, nNumOfVecs_Malignant);
				} //if ((nNumOfProcessedFiles_MalignantTot / 100) * 100 == nNumOfProcessedFiles_MalignantTot)


				///////////////////////////////////////////////////////////////////////////
			}//if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
		}//while (entry = readdir(pDIR))

		closedir(pDIR);
	}//if (pDIR = opendir("D:\\Imago\\Images\\spleen\\Malignant_AOI_50_SelecColor\\"))
	else
	{
		//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n The directory of malignant images can not be opened");

		printf("\n\n Press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);

		//perror("");
		//#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return EXIT_FAILURE;
	}//else
////////////////////////////////////////////////////////////////////

nRes = SeparabilityOf_ColorBins(

	nNumOfBins_OneImage_AllColors, //const int nNumOfBins_OneImage_AllColorsf,
	//////////////////////////////////////
	/*
		const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

	const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

	const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

	*/
	nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMin, //const int nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMinf,

	nNumOf_NonZeroMalImagesMin, //const int nNumOf_NonZeroMalImagesMinf,

	//fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin
	fRatioOfSumsOfPixelsOfNonZero_MalToNorMin, //const float fRatioOfSumsOfPixelsOfNonZero_MalToNorMinf,

	fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin, //const float fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMinf,

////////////////////////////////
	nNumOfVecs_Normal, //const int nNumOfVecs_Norf,
	nNumOfVecs_Malignant, //const int nNumOfVecs_Malf,

	nAllBinsAll_Colors_NorImagesArr, //const int nAllBinsAll_Colors_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	nAllBinsAll_Colors_MalImagesArr, //const int nAllBinsAll_Colors_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]

	fAllBinsAll_ColorsAver_NorImagesArr, //const float fAllBinsAll_ColorsAver_NorImagesArrf[], //[nNumOfBins_AllColorsForAll_NorImages]
	fAllBinsAll_ColorsAver_MalImagesArr, //const float fAllBinsAll_ColorsAver_MalImagesArrf[], //[nNumOfBins_AllColorsForAll_MalImages]
/////////////////////////////////////////////
	fBestBinSeparability, //float &fBestBinSeparabilityf,
	nNumOfBins_InNonZeroNeg_Images, //int &nNumOfBins_InNonZeroNeg_Imagesf,

	nPositOfBins_InNonZeroNeg_Images, //int nPositOfBins_InNonZeroNeg_Imagesf[], //[nNumOfBins_InNonZeroMal_ImagesMax] 

	fLargestRatioOfBinsAboveTheMinRatioForSepar); // float &fLargestRatioOfBinsAboveTheMinRatioForSeparf);

printf("\n\n nNumOfBins_InNonZeroNeg_Images = %d, nNumOfBins_InNonZeroMal_ImagesMax = %d",
	nNumOfBins_InNonZeroNeg_Images, nNumOfBins_InNonZeroMal_ImagesMax);

fprintf(fout_PrintFeatures, "\n\n nNumOfBins_InNonZeroNeg_Images = %d, nNumOfBins_InNonZeroMal_ImagesMax = %d",
	nNumOfBins_InNonZeroNeg_Images, nNumOfBins_InNonZeroMal_ImagesMax);

	for (iBin = 0; iBin < nNumOfBins_InNonZeroNeg_Images; iBin++)
	{
		printf("\n nPositOfBins_InNonZeroNeg_Images[%d] = %d", iBin, nPositOfBins_InNonZeroNeg_Images[iBin]);

		fprintf(fout_PrintFeatures, "\n nPositOfBins_InNonZeroNeg_Images[%d] = %d", iBin, nPositOfBins_InNonZeroNeg_Images[iBin]);

		nPositOfBins_InNonZeroNegCur = nPositOfBins_InNonZeroNeg_Images[iBin];
		if (nPositOfBins_InNonZeroNegCur < 0 || nPositOfBins_InNonZeroNegCur >= nNumOfBins_OneImage_AllColors)
		{
			printf("\n\n An error in 'main': nPositOfBins_InNonZeroNegCur = %d, nNumOfBins_OneImage_AllColors = %d, iBin = %d", nPositOfBins_InNonZeroNegCur, nNumOfBins_OneImage_AllColors);

			printf("\n\n Press any key to exit");	getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if (nPositOfBins_InNonZeroNegCur < 0 || nPositOfBins_InNonZeroNegCur >= nNumOfBins_OneImage_AllColors)

		nRes = Converting_nBinOfAllColorsf_To_SeparateColors(
			nPositOfBins_InNonZeroNegCur, //const int nBinOfAllColorsf,
			nRedBin, //int &nRedBinf,
			nGreenBin, //int &nGreenBinf,
			nBlueBin); // int &nBlueBinf)

		printf("\n\n The color intensity intervals for NonZero Malignant images: for Red from %d to %d, for Green from %d to %d, for Blue from %d to %d",
			nRedBin*nBinSizeForColorSepar, (nRedBin*nBinSizeForColorSepar) + nBinSizeForColorSepar,

			nGreenBin*nBinSizeForColorSepar, (nGreenBin*nBinSizeForColorSepar) + nBinSizeForColorSepar,

			nBlueBin*nBinSizeForColorSepar, (nBlueBin*nBinSizeForColorSepar) + nBinSizeForColorSepar);

		fprintf(fout_PrintFeatures, "\n\n nRedBinf = %d, nGreenBinf = %d, nBlueBinf = %d", nRedBin, nGreenBin, nBlueBin);

		fprintf(fout_PrintFeatures, "\n\n The color intensity intervals for NonZero Malignant images: for Red from %d to %d, for Green from %d to %d, for Blue from %d to %d",
			nRedBin*nBinSizeForColorSepar, (nRedBin*nBinSizeForColorSepar) + nBinSizeForColorSepar,

			nGreenBin*nBinSizeForColorSepar, (nGreenBin*nBinSizeForColorSepar) + nBinSizeForColorSepar,

			nBlueBin*nBinSizeForColorSepar, (nBlueBin*nBinSizeForColorSepar) + nBinSizeForColorSepar);

		if ((nRedBin*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
		{
			printf("\n\n An error in 'main': (nRedBin*nBinSizeForColorSepar) + nBinSizeForColorSepar = %d, iBin = %d", 
				(nRedBin*nBinSizeForColorSepar) + nBinSizeForColorSepar, iBin);

			printf("\n\n Press any key to exit");	getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if ((nRedBin*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)

		if ((nGreenBin*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
		{
			printf("\n\n An error in 'main': (nGreenBin*nBinSizeForColorSepar) + nBinSizeForColorSepar = %d, iBin = %d",
				(nGreenBin*nBinSizeForColorSepar) + nBinSizeForColorSepar, iBin);

			printf("\n\n Press any key to exit");	getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if ((nGreenBin*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)

		if ((nBlueBin*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
		{
			printf("\n\n An error in 'main': (nBlueBin*nBinSizeForColorSepar) + nBinSizeForColorSepar = %d, iBin = %d",
				(nBlueBin*nBinSizeForColorSepar) + nBinSizeForColorSepar, iBin);

			printf("\n\n Press any key to exit");	getchar(); exit(1);
			return UNSUCCESSFUL_RETURN;
		} //if ((nBlueBin*nBinSizeForColorSepar) + nBinSizeForColorSepar > nNumOfGrayLevelIntensities)
	} //for (iBin = 0; iBin < nNumOfBins_InNonZeroNeg_Images; iBin++)

printf("\n\n The end of 'main': fBestBinSeparability = %E", fBestBinSeparability);
printf("\n\n Press any key to exit");	fflush(fout_PrintFeatures);  getchar(); exit(1);

#endif //#if 0

	return SUCCESSFUL_RETURN;
} //int main()
/////////////////////////////////////
/////////////////////////////////////

//printf("\n\n Please press any key:"); getchar();