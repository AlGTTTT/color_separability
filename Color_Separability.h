#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include <vector>

#include <limits>

#include <stdint.h>

#include "image.h"
//#include "median_filter.h"

#include <dirent.h>

//#include "Cooc_Multifrac_Lacunar_Optimizer_FunctOnly.h"
#include "Color_Separability_FunctOnly.h"

//#define USING_HISTOGRAM_EQUALIZATION

//#define PRINT_HISTOGRAM_STATISTICS
//#define PRINT_ORIGINAL_FEAS

#define INCLUDE_OneBest_SeparableRatioOf_2Feas 

#define PRINT_BEST_FEAS

///////////////////////////////////////////////////////////
#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)
#define RETURN_BY_THE_SAME_INTENSITY (-3)

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define MASK_OUTPUT
//	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax_La (2048) //2048
#define nWidMax_La (2048) //2048 

#define nLenMin_La 50 
#define nWidMin_La 50

//#define nImageAreaMax (nLenMax_La*nWidMax_La)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535
//////////////////////////////////////////////////////

	#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000
#define nWidMax 6000

#define nLenMin 50
#define nWidMin 50

#define nImageAreaMax (nLenMax*nWidMax)
#define nImageAreaMin (nLenMin*nWidMin)

///#define nLenSubimageToPrintMin  0 //342
//#define nLenSubimageToPrintMax  4000  //2500

//#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470

//#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970

///////////////////////////////////////////////////////////////////////////////

#define nNumOfGrayLevelIntensities 256 // (nGrayLevelIntensityMax + 1)

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85
#define nIntensityStatMax 255

#define nIntensityStatWhite (nIntensityStatMax)
/////////////////////////////////////////////////////////////

#define nPositionOf_TotFeaToPrint (-1)

#define fWeightOfRed_InStr 1.0 // 0.0
#define fWeightOfGreen_InStr 1.0
#define fWeightOfBlue_InStr 1.0 //0.0 //

/////////////////////////////////////////////////////
//#define nNumOfVecs_Normal 50 //after removing AoIs with dim > nDim_2pN_Max_La == 2048 // D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Benign\\AOI\\

#define nNumOfVecs_Normal 500 //D:\\Imago\\Images\\WoodyBreast\\Color_Images\\Normal-COBB-AOI\\ \\D:\Imago\Images\WoodyBreast\AOIs\Normal-Panoramic

//#define nNumOfVecs_Normal 227 //D:\Imago\Images\spleen\Color\BenignAOI-Color-PNG
//////////////////////////////

//#define nNumOfVecs_Malignant 575  //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

//#define nNumOfVecs_Malignant 50 //D:\\Imago\\Images\\spleen\\Genas_Images_June26\\Malignant\\AOI\\

#define nNumOfVecs_Malignant 298 //D:\\Imago\\Images\\WoodyBreast\\Color_Images\\WB-Color-AOI //D:\Imago\Images\WoodyBreast\AOIs\WB-Panoramic

//#define nNumOfVecs_Malignant 311 //D:\Imago\Images\spleen\Color\MalignantAOI-Color-PNG

////////////////////////////////////////////////////////////
//watch for '#define USING_HISTOGRAM_EQUALIZATION'

//////////////////////////////////////////////////////////////
#define nNumOfSelectedFea (-1) //2247 // >= nNumOfFeasForMultifractalTot and < 2*nNumOfFeasForMultifractalTot
//////////////////////////////////////////////////////

//#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

//#define nNumOfHistogramBinsStat (nNumOfGrayLevelIntensities) //(nIntensityStatMax - nIntensityStatMin + 1)

//#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

/////////////////////////////////////////////
//nBinSizeForColorSepar = 1,2 could be added as needed
//#define nBinSizeForColorSepar 2 //,8,...
//#define nBinSizeForColorSepar 4 //,8,...
//#define nBinSizeForColorSepar 8 //,...
//#define nBinSizeForColorSepar 16 //,...
#define nBinSizeForColorSepar 32 //,...

#define nNumOfBins_OneColor (nNumOfGrayLevelIntensities/nBinSizeForColorSepar) //256/2
//#define nNumOfBins_OneColor 8

//#define nSquareOfNumOfBins_OneColor (nNumOfBins_OneColor*nNumOfBins_OneColor)

#define nNumOfBins_OneImage_AllColors (nNumOfBins_OneColor*nNumOfBins_OneColor*nNumOfBins_OneColor) 
//#define nNumOfBins_OneImage_AllColors 512//(8^3) 


#define nIntensityOfBackground 255 // or 0 --check it up

#define nBinIntensityOfBackground (nIntensityOfBackground/nBinSizeForColorSepar) // or 0 --check it up

#define nNumOfOneColorBinsInAll_NorImages (nNumOfVecs_Normal * nNumOfBins_OneColor)
//#define nNumOfOneColorBinsInAll_NorImages 400 //(50 * 8)

#define nNumOfOneColorBinsInAll_MalImages (nNumOfVecs_Malignant * nNumOfBins_OneColor)
//#define nNumOfOneColorBinsInAll_MalImages 400 //(50 * 8)

#define nNumOfBins_AllColorsForAll_NorImages (nNumOfBins_OneImage_AllColors * nNumOfVecs_Normal)
//#define nNumOfBins_AllColorsForAll_NorImages 25600 //512*50

#define nNumOfBins_AllColorsForAll_MalImages (nNumOfBins_OneImage_AllColors * nNumOfVecs_Malignant)
//#define nNumOfBins_AllColorsForAll_MalImages 25600  //512*50
////////////////////////////////////////////////////////
//separability of non-zero bins

#define nNumOfPixelsIn_ABin_OfNonZeroImage_ForSeparMin 20

#define nNumOf_NonZeroMalImagesMin 2 //4 //< nNumOfVecs_Malignant

//#define fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin 2.5 //4.0
#define fRatioOfNumOf_NonZeroMalToNorImages_For_ABinMin 5.0

//#define fRatioOfSumsOfPixelsOfNonZero_MalToNorMin 4.0 //10.0
#define fRatioOfSumsOfPixelsOfNonZero_MalToNorMin 10.0

		//fRatioOfNumOfPixelsIn_ABin_InANonZeroImage_ForSeparMin
//#define fRatioOfNumOf_NonZeroMalToNorImages_For_ABinForSeparMin 5.0

//for nPositOfBinsAboveTheMinRatio_InNonZeroNeg_Images_ForSeparArrf[[]
#define nNumOfBins_InNonZeroMal_ImagesMax 100

////////////////////////////////////////////////////////////////////////
#define nNumOfBinCurOfInterest (-1)
///////////////////////////////
#define fEfficBestSeparOfOneFeaAcceptMax (-1.01) //the min at the perfect separation is (-2.0) 

//#define nNumVecTrain_1st (nNumOf_TrainVecs_Normal) //10 

//#define nNumVecTrain_2nd (nNumOf_TrainVecs_Malignant) //10 //(nNumOf_TrainVecs_Normal) //1000 //100

//#define nProdTrain_1st (nDim*nNumOf_TrainVecs_Normal)

//#define nProdTrain_2nd (nDim*nNumVecTrain_2nd)

//#define nNumIterForDataGenerMax (nNumOf_TrainVecs_Normal*50)

#define nNumDistiguishFeasForInit 2
/////////////////////////////////////////////////
#define fEfficiency_ToPrint (-1.5)

#define fThreshold_HighSeparability 0.5
#define fThreshold_LowSeparability_1 0.3
#define fThreshold_LowSeparability_2 0.7
/////////////////////////////////////////////////
#define fBorderBel 0.0
#define fBorderAbove 1.0

#define nNumIterMaxForFindingBestSeparByRatioOfOneFea 10 //100
#define nNumIterMaxForFindingBestSeparByDiffOfOneFea (nNumIterMaxForFindingBestSeparByRatioOfOneFea)

////////////////////////////////////
/*
#define fC_Const 6.18034E-01 //( (-1.0 + sqrt(5.0) )/2.0 ) 
#define fR_Const 3.81966E-01 //(1.0 - fC_Const)

#define fEfficOfArr1stRequired 0.9

#define fFeaValueThreshold_ForWarning 1.0E+8 //6

const float fPrecisionOf_G_Const_Search = (float)(0.01); // 0.005
*/
//////////////////////////////////////////////

typedef struct
{
	float 
		fWeightOfRed; //<= 1.0
	float
		fWeightOfGreen; //<= 1.0
	float
		fWeightOfBlue; //<= 1.0

} WEIGHTES_OF_RGB_COLORS;

/////////////////////////////////////////////////////////

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;

////////////////////////////////////////////////////////////////
//cooc feas
typedef struct
{
	int nIndicOfClass;

	int nLength; // nLenCur; //<= nLenMax
	int nWidth; // nWidCur; //<= nWidMax

	//int nPixelArr[nImageAreaMax];
	int *nPixelArr; //Grayscale_Imagef->nPixelArr[nIndexOfPixelCurf] = (nRedf + nGreenf + nBluef)/3;

} GRAYSCALE_IMAGE;

///////////////////////////////


//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 1 + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )
//#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (4 + nNumOfHistogramIntervals_ForCooc + nNumOfHistogramIntervals_ForCooc - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 10) + (nNumOfScalesAndDistTot*10) )

//
/////////////////////////////////////////////////////////////





